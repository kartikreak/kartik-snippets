/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
inspection_date();
function inspection_date()
{

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = dd + '/' + mm + '/' + yyyy;

document.getElementById("inspection_date").value = today;
    
}
function electrical_power_f(overhead)
{
    
    if (overhead == "overhead") 
    {
        document.getElementById("overhead_mast_show").style.display = "block";
    }
    else
    {
        document.getElementById("overhead_mast_show").style.display = "none";
    }    
        
}

/* 
 * Values by Property name
 */



/* 
 *  Checkboxes values
 */

$("#isgcc_file").change(function()
{
    if($(this).is(":checked"))
    {
        document.getElementById("gcc_file").disabled = true;
    }
    else
    {
        document.getElementById("gcc_file").disabled = false;
    }
});


$("#isrmf_id_").change(function(){if($(this).is(":checked")){ document.getElementById("rmf_id_").disabled = true;}else{document.getElementById("rmf_id_").disabled = false;}});
$("#isbldg_").change(function(){if($(this).is(":checked")){ document.getElementById("bldg_").disabled = true;}else{document.getElementById("bldg_").disabled = false;}});
$("#isof").change(function(){if($(this).is(":checked")){ document.getElementById("of").disabled = true;}else{document.getElementById("of").disabled = false;}});
$("#isfraternity").change(function(){if($(this).is(":checked")){ document.getElementById("fraternity").disabled = true;}else{document.getElementById("fraternity").disabled = false;}});
$("#isuniversity").change(function(){if($(this).is(":checked")){ document.getElementById("university").disabled = true;}else{document.getElementById("university").disabled = false;}});
$("#isaddress").change(function(){if($(this).is(":checked")){ document.getElementById("address").disabled = true;}else{document.getElementById("address").disabled = false;}});
$("#iscity").change(function(){if($(this).is(":checked")){ document.getElementById("city").disabled = true;}else{document.getElementById("city").disabled = false;}});
$("#isstate").change(function(){if($(this).is(":checked")){ document.getElementById("state").disabled = true;}else{document.getElementById("state").disabled = false;}});
$("#iszip_code").change(function(){if($(this).is(":checked")){ document.getElementById("zip_code").disabled = true;}else{document.getElementById("zip_code").disabled = false;}});
$("#isgcc_valuation").change(function(){if($(this).is(":checked")){ document.getElementById("gcc_valuation").disabled = true;}else{document.getElementById("gcc_valuation").disabled = false;}});
$("#isinspected").change(function(){if($(this).is(":checked")){ document.getElementById("inspected").disabled = true;}else{document.getElementById("inspected").disabled = false;}});
$("#islast_inspection").change(function(){if($(this).is(":checked")){ document.getElementById("last_inspection").disabled = true;}else{document.getElementById("last_inspection").disabled = false;}});
$("#isstatus").change(function(){if($(this).is(":checked")){ document.getElementById("status").disabled = true;}else{document.getElementById("status").disabled = false;}});
$("#isyear_built").change(function(){if($(this).is(":checked")){ document.getElementById("year_built").disabled = true;}else{document.getElementById("year_built").disabled = false;}});
$("#isremodeled").change(function(){if($(this).is(":checked")){ document.getElementById("remodeled").disabled = true;}else{document.getElementById("remodeled").disabled = false;}});
$("#issq_ft").change(function(){if($(this).is(":checked")){ document.getElementById("sq_ft").disabled = true;}else{document.getElementById("sq_ft").disabled = false;}});
$("#isbldg_stories").change(function(){if($(this).is(":checked")){ document.getElementById("bldg_stories").disabled = true;}else{document.getElementById("bldg_stories").disabled = false;}});
$("#isconst_type").change(function(){if($(this).is(":checked")){ document.getElementById("const_type").disabled = true;}else{document.getElementById("const_type").disabled = false;}});
$("#isiso_prot_type").change(function(){if($(this).is(":checked")){ document.getElementById("iso_prot_type").disabled = true;}else{document.getElementById("iso_prot_type").disabled = false;}});
$("#ismax_occup").change(function(){if($(this).is(":checked")){ document.getElementById("max_occup").disabled = true;}else{document.getElementById("max_occup").disabled = false;}});
$("#issprinklers1").change(function(){if($(this).is(":checked")){ document.getElementById("sprinklers1").disabled = true;}else{document.getElementById("sprinklers1").disabled = false;}});
$("#iscurrent_ms_policy_limit").change(function(){if($(this).is(":checked")){ document.getElementById("current_ms_policy_limit").disabled = true;}else{document.getElementById("current_ms_policy_limit").disabled = false;}});
$("#islosses_paid_20002015").change(function(){if($(this).is(":checked")){ document.getElementById("losses_paid_20002015").disabled = true;}else{document.getElementById("losses_paid_20002015").disabled = false;}});
$("#issfinterview").change(function(){if($(this).is(":checked")){ document.getElementById("sfinterview").disabled = true;}else{document.getElementById("sfinterview").disabled = false;}});
$("#iscurrent_occup").change(function(){if($(this).is(":checked")){ document.getElementById("current_occup").disabled = true;}else{document.getElementById("current_occup").disabled = false;}});
$("#isqty_dormitories").change(function(){if($(this).is(":checked")){ document.getElementById("qty_dormitories").disabled = true;}else{document.getElementById("qty_dormitories").disabled = false;}});
$("#islibrarystudy").change(function(){if($(this).is(":checked")){ document.getElementById("librarystudy").disabled = true;}else{document.getElementById("librarystudy").disabled = false;}});
$("#isdate").change(function(){if($(this).is(":checked")){ document.getElementById("date").disabled = true;}else{document.getElementById("date").disabled = false;}});
$("#isbuilding_interview_completed").change(function(){if($(this).is(":checked")){ document.getElementById("building_interview_completed").disabled = true;}else{document.getElementById("building_interview_completed").disabled = false;}});
$("#issftax_records").change(function(){if($(this).is(":checked")){ document.getElementById("sftax_records").disabled = true;}else{document.getElementById("sftax_records").disabled = false;}});
$("#issfvalucation").change(function(){if($(this).is(":checked")){ document.getElementById("sfvalucation").disabled = true;}else{document.getElementById("sfvalucation").disabled = false;}});
$("#ismax_occupants").change(function(){if($(this).is(":checked")){ document.getElementById("max_occupants").disabled = true;}else{document.getElementById("max_occupants").disabled = false;}});
$("#isbldg_occup").change(function(){if($(this).is(":checked")){ document.getElementById("bldg_occup").disabled = true;}else{document.getElementById("bldg_occup").disabled = false;}});
$("#isdormitory_type").change(function(){if($(this).is(":checked")){ document.getElementById("dormitory_type").disabled = true;}else{document.getElementById("dormitory_type").disabled = false;}});
$("#iskitchen_type").change(function(){if($(this).is(":checked")){ document.getElementById("kitchen_type").disabled = true;}else{document.getElementById("kitchen_type").disabled = false;}});
$("#isbuilding_type__iso").change(function(){if($(this).is(":checked")){ document.getElementById("building_type__iso").disabled = true;}else{document.getElementById("building_type__iso").disabled = false;}});
$("#isexterior_wall_finish").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_wall_finish").disabled = true;}else{document.getElementById("exterior_wall_finish").disabled = false;}});
$("#isbuilding_stories").change(function(){if($(this).is(":checked")){ document.getElementById("building_stories").disabled = true;}else{document.getElementById("building_stories").disabled = false;}});
$("#isbuilding_basement").change(function(){if($(this).is(":checked")){ document.getElementById("building_basement").disabled = true;}else{document.getElementById("building_basement").disabled = false;}});
$("#isfloor_height1").change(function(){if($(this).is(":checked")){ document.getElementById("floor_height1").disabled = true;}else{document.getElementById("floor_height1").disabled = false;}});
$("#isupper_flr_constr").change(function(){if($(this).is(":checked")){ document.getElementById("upper_flr_constr").disabled = true;}else{document.getElementById("upper_flr_constr").disabled = false;}});
$("#issprinklers2").change(function(){if($(this).is(":checked")){ document.getElementById("sprinklers2").disabled = true;}else{document.getElementById("sprinklers2").disabled = false;}});
$("#isfire_hydrants").change(function(){if($(this).is(":checked")){ document.getElementById("fire_hydrants").disabled = true;}else{document.getElementById("fire_hydrants").disabled = false;}});
$("#isfire_alarm_system").change(function(){if($(this).is(":checked")){ document.getElementById("fire_alarm_system").disabled = true;}else{document.getElementById("fire_alarm_system").disabled = false;}});
$("#iscctv_system").change(function(){if($(this).is(":checked")){ document.getElementById("cctv_system").disabled = true;}else{document.getElementById("cctv_system").disabled = false;}});
$("#iscctv_30_day_recording").change(function(){if($(this).is(":checked")){ document.getElementById("cctv_30_day_recording").disabled = true;}else{document.getElementById("cctv_30_day_recording").disabled = false;}});
$("#iscctv_credit_qualified").change(function(){if($(this).is(":checked")){ document.getElementById("cctv_credit_qualified").disabled = true;}else{document.getElementById("cctv_credit_qualified").disabled = false;}});
$("#isdoor_entry_system").change(function(){if($(this).is(":checked")){ document.getElementById("door_entry_system").disabled = true;}else{document.getElementById("door_entry_system").disabled = false;}});
$("#isexterior_doors_with_system").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_doors_with_system").disabled = true;}else{document.getElementById("exterior_doors_with_system").disabled = false;}});
$("#isdoor_entry_credit_qualified").change(function(){if($(this).is(":checked")){ document.getElementById("door_entry_credit_qualified").disabled = true;}else{document.getElementById("door_entry_credit_qualified").disabled = false;}});
$("#isroof_type").change(function(){if($(this).is(":checked")){ document.getElementById("roof_type").disabled = true;}else{document.getElementById("roof_type").disabled = false;}});
$("#isroof_farming").change(function(){if($(this).is(":checked")){ document.getElementById("roof_farming").disabled = true;}else{document.getElementById("roof_farming").disabled = false;}});
$("#isroof_decking").change(function(){if($(this).is(":checked")){ document.getElementById("roof_decking").disabled = true;}else{document.getElementById("roof_decking").disabled = false;}});
$("#isroof_covering").change(function(){if($(this).is(":checked")){ document.getElementById("roof_covering").disabled = true;}else{document.getElementById("roof_covering").disabled = false;}});
$("#isleaks_present").change(function(){if($(this).is(":checked")){ document.getElementById("leaks_present").disabled = true;}else{document.getElementById("leaks_present").disabled = false;}});
$("#isroof_in_warranty").change(function(){if($(this).is(":checked")){ document.getElementById("roof_in_warranty").disabled = true;}else{document.getElementById("roof_in_warranty").disabled = false;}});
$("#isaction_needed").change(function(){if($(this).is(":checked")){ document.getElementById("action_needed").disabled = true;}else{document.getElementById("action_needed").disabled = false;}});
$("#isexterior_site_conditions_completed").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_site_conditions_completed").disabled = true;}else{document.getElementById("exterior_site_conditions_completed").disabled = false;}});
$("#isexterior_life_safety_completed").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_life_safety_completed").disabled = true;}else{document.getElementById("exterior_life_safety_completed").disabled = false;}});
$("#isexterior_fire_protection_completed").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_fire_protection_completed").disabled = true;}else{document.getElementById("exterior_fire_protection_completed").disabled = false;}});
$("#isis_address_visible_from_curb").change(function(){if($(this).is(":checked")){ document.getElementById("is_address_visible_from_curb").disabled = true;}else{document.getElementById("is_address_visible_from_curb").disabled = false;}});
$("#isare_fire_hydrants_visible_from_property").change(function(){if($(this).is(":checked")){ document.getElementById("are_fire_hydrants_visible_from_property").disabled = true;}else{document.getElementById("are_fire_hydrants_visible_from_property").disabled = false;}});
$("#iswhat_is_distance_to_visible_fire_hydrant").change(function(){if($(this).is(":checked")){ document.getElementById("what_is_distance_to_visible_fire_hydrant").disabled = true;}else{document.getElementById("what_is_distance_to_visible_fire_hydrant").disabled = false;}});
$("#isare_cctv_cameras_at_all_doors").change(function(){if($(this).is(":checked")){ document.getElementById("are_cctv_cameras_at_all_doors").disabled = true;}else{document.getElementById("are_cctv_cameras_at_all_doors").disabled = false;}});
$("#isare_all_doors_equipped_with_electronic_access").change(function(){if($(this).is(":checked")){ document.getElementById("are_all_doors_equipped_with_electronic_access").disabled = true;}else{document.getElementById("are_all_doors_equipped_with_electronic_access").disabled = false;}});
$("#isknox_box_installed_for_fire_department").change(function(){if($(this).is(":checked")){ document.getElementById("knox_box_installed_for_fire_department").disabled = true;}else{document.getElementById("knox_box_installed_for_fire_department").disabled = false;}});
$("#ispower_to_building").change(function(){if($(this).is(":checked")){ document.getElementById("power_to_building").disabled = true;}else{document.getElementById("power_to_building").disabled = false;}});
$("#istype_of_backup_power").change(function(){if($(this).is(":checked")){ document.getElementById("type_of_backup_power").disabled = true;}else{document.getElementById("type_of_backup_power").disabled = false;}});
$("#istype_of_water_supply").change(function(){if($(this).is(":checked")){ document.getElementById("type_of_water_supply").disabled = true;}else{document.getElementById("type_of_water_supply").disabled = false;}});
$("#istype_of_heating_fuel").change(function(){if($(this).is(":checked")){ document.getElementById("type_of_heating_fuel").disabled = true;}else{document.getElementById("type_of_heating_fuel").disabled = false;}});
$("#istype_of_sewer_discharge").change(function(){if($(this).is(":checked")){ document.getElementById("type_of_sewer_discharge").disabled = true;}else{document.getElementById("type_of_sewer_discharge").disabled = false;}});
$("#issite_exit__sidewalks_obstructed").change(function(){if($(this).is(":checked")){ document.getElementById("site_exit__sidewalks_obstructed").disabled = true;}else{document.getElementById("site_exit__sidewalks_obstructed").disabled = false;}});
$("#issite_exit__paths_clear").change(function(){if($(this).is(":checked")){ document.getElementById("site_exit__paths_clear").disabled = true;}else{document.getElementById("site_exit__paths_clear").disabled = false;}});
$("#issite_ada_parking_availble").change(function(){if($(this).is(":checked")){ document.getElementById("site_ada_parking_availble").disabled = true;}else{document.getElementById("site_ada_parking_availble").disabled = false;}});
$("#issite_exterior_lighting").change(function(){if($(this).is(":checked")){ document.getElementById("site_exterior_lighting").disabled = true;}else{document.getElementById("site_exterior_lighting").disabled = false;}});
$("#issite_ada_ramp_avaible").change(function(){if($(this).is(":checked")){ document.getElementById("site_ada_ramp_avaible").disabled = true;}else{document.getElementById("site_ada_ramp_avaible").disabled = false;}});
$("#issite_parking_area_type").change(function(){if($(this).is(":checked")){ document.getElementById("site_parking_area_type").disabled = true;}else{document.getElementById("site_parking_area_type").disabled = false;}});
$("#issite__fencing_present").change(function(){if($(this).is(":checked")){ document.getElementById("site__fencing_present").disabled = true;}else{document.getElementById("site__fencing_present").disabled = false;}});
$("#isare_downspouts_directed_away_from_buildingnone_present").change(function(){if($(this).is(":checked")){ document.getElementById("are_downspouts_directed_away_from_buildingnone_present").disabled = true;}else{document.getElementById("are_downspouts_directed_away_from_buildingnone_present").disabled = false;}});
$("#isdownspout_discharge_typeon_ground").change(function(){if($(this).is(":checked")){ document.getElementById("downspout_discharge_typeon_ground").disabled = true;}else{document.getElementById("downspout_discharge_typeon_ground").disabled = false;}});
$("#issite_susceptible_to_floodsno").change(function(){if($(this).is(":checked")){ document.getElementById("site_susceptible_to_floodsno").disabled = true;}else{document.getElementById("site_susceptible_to_floodsno").disabled = false;}});
$("#isdoes_grade_slope_away_from_foundationno").change(function(){if($(this).is(":checked")){ document.getElementById("does_grade_slope_away_from_foundationno").disabled = true;}else{document.getElementById("does_grade_slope_away_from_foundationno").disabled = false;}});
$("#issite_condition_commentstextbox").change(function(){if($(this).is(":checked")){ document.getElementById("site_condition_commentstextbox").disabled = true;}else{document.getElementById("site_condition_commentstextbox").disabled = false;}});
$("#issprinkler_system_presentnone").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_system_presentnone").disabled = true;}else{document.getElementById("sprinkler_system_presentnone").disabled = false;}});
$("#isis_the_sprinkler_fdc_properly_identifiednone").change(function(){if($(this).is(":checked")){ document.getElementById("is_the_sprinkler_fdc_properly_identifiednone").disabled = true;}else{document.getElementById("is_the_sprinkler_fdc_properly_identifiednone").disabled = false;}});
$("#isare_the_fdc_caps_in_placenone").change(function(){if($(this).is(":checked")){ document.getElementById("are_the_fdc_caps_in_placenone").disabled = true;}else{document.getElementById("are_the_fdc_caps_in_placenone").disabled = false;}});
$("#isis_fire_alarm_gong_mounted_on_exterior_wallyes").change(function(){if($(this).is(":checked")){ document.getElementById("is_fire_alarm_gong_mounted_on_exterior_wallyes").disabled = true;}else{document.getElementById("is_fire_alarm_gong_mounted_on_exterior_wallyes").disabled = false;}});
$("#issprinkler_system_commentstextbox").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_system_commentstextbox").disabled = true;}else{document.getElementById("sprinkler_system_commentstextbox").disabled = false;}});
$("#isare_emergency_exist_clearyes").change(function(){if($(this).is(":checked")){ document.getElementById("are_emergency_exist_clearyes").disabled = true;}else{document.getElementById("are_emergency_exist_clearyes").disabled = false;}});
$("#isare_any_vehicle_parking_spaces_blocking_egress_pathsno").change(function(){if($(this).is(":checked")){ document.getElementById("are_any_vehicle_parking_spaces_blocking_egress_pathsno").disabled = true;}else{document.getElementById("are_any_vehicle_parking_spaces_blocking_egress_pathsno").disabled = false;}});
$("#isare_any_flammable_fuels_sotred_close_to_buildingyes").change(function(){if($(this).is(":checked")){ document.getElementById("are_any_flammable_fuels_sotred_close_to_buildingyes").disabled = true;}else{document.getElementById("are_any_flammable_fuels_sotred_close_to_buildingyes").disabled = false;}});
$("#isis_a_4hr_fire_rated_shed_presentyes").change(function(){if($(this).is(":checked")){ document.getElementById("is_a_4hr_fire_rated_shed_presentyes").disabled = true;}else{document.getElementById("is_a_4hr_fire_rated_shed_presentyes").disabled = false;}});
$("#isare_public_areas_well_maintainedno").change(function(){if($(this).is(":checked")){ document.getElementById("are_public_areas_well_maintainedno").disabled = true;}else{document.getElementById("are_public_areas_well_maintainedno").disabled = false;}});
$("#isare_doors_and_windows_in_good_conditionno").change(function(){if($(this).is(":checked")){ document.getElementById("are_doors_and_windows_in_good_conditionno").disabled = true;}else{document.getElementById("are_doors_and_windows_in_good_conditionno").disabled = false;}});
$("#isare_extension_cords_being_usedno").change(function(){if($(this).is(":checked")){ document.getElementById("are_extension_cords_being_usedno").disabled = true;}else{document.getElementById("are_extension_cords_being_usedno").disabled = false;}});
$("#isis_exterior_security_lighting_availbleno").change(function(){if($(this).is(":checked")){ document.getElementById("is_exterior_security_lighting_availbleno").disabled = true;}else{document.getElementById("is_exterior_security_lighting_availbleno").disabled = false;}});
$("#isis_there_barbeque_equipments_close_to_buildingno").change(function(){if($(this).is(":checked")){ document.getElementById("is_there_barbeque_equipments_close_to_buildingno").disabled = true;}else{document.getElementById("is_there_barbeque_equipments_close_to_buildingno").disabled = false;}});
$("#isare_any_fire_pits_on_prorpertyno").change(function(){if($(this).is(":checked")){ document.getElementById("are_any_fire_pits_on_prorpertyno").disabled = true;}else{document.getElementById("are_any_fire_pits_on_prorpertyno").disabled = false;}});
$("#isis_there_a_fire_extinguisher_availbleno").change(function(){if($(this).is(":checked")){ document.getElementById("is_there_a_fire_extinguisher_availbleno").disabled = true;}else{document.getElementById("is_there_a_fire_extinguisher_availbleno").disabled = false;}});
$("#isare_there_any_athletic_fields_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("are_there_any_athletic_fields_presentno").disabled = true;}else{document.getElementById("are_there_any_athletic_fields_presentno").disabled = false;}});
$("#isexterior_commentstextbox").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_commentstextbox").disabled = true;}else{document.getElementById("exterior_commentstextbox").disabled = false;}});
$("#isinterior_inspected").change(function(){if($(this).is(":checked")){ document.getElementById("interior_inspected").disabled = true;}else{document.getElementById("interior_inspected").disabled = false;}});
$("#isinterior_fire_protection_inspected").change(function(){if($(this).is(":checked")){ document.getElementById("interior_fire_protection_inspected").disabled = true;}else{document.getElementById("interior_fire_protection_inspected").disabled = false;}});
$("#isinterior_life_safety_inspected").change(function(){if($(this).is(":checked")){ document.getElementById("interior_life_safety_inspected").disabled = true;}else{document.getElementById("interior_life_safety_inspected").disabled = false;}});
$("#isfloor_height2").change(function(){if($(this).is(":checked")){ document.getElementById("floor_height2").disabled = true;}else{document.getElementById("floor_height2").disabled = false;}});
$("#iscctv_systemnone").change(function(){if($(this).is(":checked")){ document.getElementById("cctv_systemnone").disabled = true;}else{document.getElementById("cctv_systemnone").disabled = false;}});
$("#iscctv_30_day_recordno").change(function(){if($(this).is(":checked")){ document.getElementById("cctv_30_day_recordno").disabled = true;}else{document.getElementById("cctv_30_day_recordno").disabled = false;}});
$("#iscctv_chi_credit_permittedno").change(function(){if($(this).is(":checked")){ document.getElementById("cctv_chi_credit_permittedno").disabled = true;}else{document.getElementById("cctv_chi_credit_permittedno").disabled = false;}});
$("#issecondary_floor_constructionnot_applicable").change(function(){if($(this).is(":checked")){ document.getElementById("secondary_floor_constructionnot_applicable").disabled = true;}else{document.getElementById("secondary_floor_constructionnot_applicable").disabled = false;}});
$("#issprinklersyes").change(function(){if($(this).is(":checked")){ document.getElementById("sprinklersyes").disabled = true;}else{document.getElementById("sprinklersyes").disabled = false;}});
$("#isfire_safety_plan_availableno").change(function(){if($(this).is(":checked")){ document.getElementById("fire_safety_plan_availableno").disabled = true;}else{document.getElementById("fire_safety_plan_availableno").disabled = false;}});
$("#isdoor_entry_systemkey").change(function(){if($(this).is(":checked")){ document.getElementById("door_entry_systemkey").disabled = true;}else{document.getElementById("door_entry_systemkey").disabled = false;}});
$("#isevacuation_plans_postedno").change(function(){if($(this).is(":checked")){ document.getElementById("evacuation_plans_postedno").disabled = true;}else{document.getElementById("evacuation_plans_postedno").disabled = false;}});
$("#isdoors_with_systemkey").change(function(){if($(this).is(":checked")){ document.getElementById("doors_with_systemkey").disabled = true;}else{document.getElementById("doors_with_systemkey").disabled = false;}});
$("#isfire_alarm_systemnone").change(function(){if($(this).is(":checked")){ document.getElementById("fire_alarm_systemnone").disabled = true;}else{document.getElementById("fire_alarm_systemnone").disabled = false;}});
$("#isentry_chi_credit_permittedno").change(function(){if($(this).is(":checked")){ document.getElementById("entry_chi_credit_permittedno").disabled = true;}else{document.getElementById("entry_chi_credit_permittedno").disabled = false;}});
$("#isis_alarm_system_under_contractno").change(function(){if($(this).is(":checked")){ document.getElementById("is_alarm_system_under_contractno").disabled = true;}else{document.getElementById("is_alarm_system_under_contractno").disabled = false;}});
$("#isfire_alarm_panel").change(function(){if($(this).is(":checked")){ document.getElementById("fire_alarm_panel").disabled = true;}else{document.getElementById("fire_alarm_panel").disabled = false;}});
$("#isfire_alarm_panel_monitored").change(function(){if($(this).is(":checked")){ document.getElementById("fire_alarm_panel_monitored").disabled = true;}else{document.getElementById("fire_alarm_panel_monitored").disabled = false;}});
$("#issprinkler_system").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_system").disabled = true;}else{document.getElementById("sprinkler_system").disabled = false;}});
$("#issprinkler_system_monitored").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_system_monitored").disabled = true;}else{document.getElementById("sprinkler_system_monitored").disabled = false;}});
$("#issprinkler_riser").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_riser").disabled = true;}else{document.getElementById("sprinkler_riser").disabled = false;}});
$("#isriser_valves_open").change(function(){if($(this).is(":checked")){ document.getElementById("riser_valves_open").disabled = true;}else{document.getElementById("riser_valves_open").disabled = false;}});
$("#ispressure_in_system").change(function(){if($(this).is(":checked")){ document.getElementById("pressure_in_system").disabled = true;}else{document.getElementById("pressure_in_system").disabled = false;}});
$("#isgauges_working").change(function(){if($(this).is(":checked")){ document.getElementById("gauges_working").disabled = true;}else{document.getElementById("gauges_working").disabled = false;}});
$("#isflow_controls_presents").change(function(){if($(this).is(":checked")){ document.getElementById("flow_controls_presents").disabled = true;}else{document.getElementById("flow_controls_presents").disabled = false;}});
$("#isinspector_drain_lines").change(function(){if($(this).is(":checked")){ document.getElementById("inspector_drain_lines").disabled = true;}else{document.getElementById("inspector_drain_lines").disabled = false;}});
$("#isfire_extinguishers_present").change(function(){if($(this).is(":checked")){ document.getElementById("fire_extinguishers_present").disabled = true;}else{document.getElementById("fire_extinguishers_present").disabled = false;}});
$("#isfire_extinguishers_tagged").change(function(){if($(this).is(":checked")){ document.getElementById("fire_extinguishers_tagged").disabled = true;}else{document.getElementById("fire_extinguishers_tagged").disabled = false;}});
$("#isis_extinguisher_hydro_testing_required").change(function(){if($(this).is(":checked")){ document.getElementById("is_extinguisher_hydro_testing_required").disabled = true;}else{document.getElementById("is_extinguisher_hydro_testing_required").disabled = false;}});
$("#isextinguishers_installed").change(function(){if($(this).is(":checked")){ document.getElementById("extinguishers_installed").disabled = true;}else{document.getElementById("extinguishers_installed").disabled = false;}});
$("#issmoke_detectors_present").change(function(){if($(this).is(":checked")){ document.getElementById("smoke_detectors_present").disabled = true;}else{document.getElementById("smoke_detectors_present").disabled = false;}});
$("#issmoke_detector_age").change(function(){if($(this).is(":checked")){ document.getElementById("smoke_detector_age").disabled = true;}else{document.getElementById("smoke_detector_age").disabled = false;}});
$("#issmoke_detector_monitored").change(function(){if($(this).is(":checked")){ document.getElementById("smoke_detector_monitored").disabled = true;}else{document.getElementById("smoke_detector_monitored").disabled = false;}});
$("#issmoke_detector_tested").change(function(){if($(this).is(":checked")){ document.getElementById("smoke_detector_tested").disabled = true;}else{document.getElementById("smoke_detector_tested").disabled = false;}});
$("#isco_detectors_present").change(function(){if($(this).is(":checked")){ document.getElementById("co_detectors_present").disabled = true;}else{document.getElementById("co_detectors_present").disabled = false;}});
$("#isco_detector_age").change(function(){if($(this).is(":checked")){ document.getElementById("co_detector_age").disabled = true;}else{document.getElementById("co_detector_age").disabled = false;}});
$("#isco_detectors_monitored").change(function(){if($(this).is(":checked")){ document.getElementById("co_detectors_monitored").disabled = true;}else{document.getElementById("co_detectors_monitored").disabled = false;}});
$("#isco_detector_tested").change(function(){if($(this).is(":checked")){ document.getElementById("co_detector_tested").disabled = true;}else{document.getElementById("co_detector_tested").disabled = false;}});
$("#isalarm_hornstrobes_present").change(function(){if($(this).is(":checked")){ document.getElementById("alarm_hornstrobes_present").disabled = true;}else{document.getElementById("alarm_hornstrobes_present").disabled = false;}});
$("#isspare_heads_present").change(function(){if($(this).is(":checked")){ document.getElementById("spare_heads_present").disabled = true;}else{document.getElementById("spare_heads_present").disabled = false;}});
$("#isare_alarm_pulls_present").change(function(){if($(this).is(":checked")){ document.getElementById("are_alarm_pulls_present").disabled = true;}else{document.getElementById("are_alarm_pulls_present").disabled = false;}});
$("#issprinkler_pole_available").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_pole_available").disabled = true;}else{document.getElementById("sprinkler_pole_available").disabled = false;}});
$("#isdry_chemical_system").change(function(){if($(this).is(":checked")){ document.getElementById("dry_chemical_system").disabled = true;}else{document.getElementById("dry_chemical_system").disabled = false;}});
$("#isare_sprinkler_heads_visible").change(function(){if($(this).is(":checked")){ document.getElementById("are_sprinkler_heads_visible").disabled = true;}else{document.getElementById("are_sprinkler_heads_visible").disabled = false;}});
$("#isantifreezedry_system_present").change(function(){if($(this).is(":checked")){ document.getElementById("antifreezedry_system_present").disabled = true;}else{document.getElementById("antifreezedry_system_present").disabled = false;}});
$("#isfire_protection_comments").change(function(){if($(this).is(":checked")){ document.getElementById("fire_protection_comments").disabled = true;}else{document.getElementById("fire_protection_comments").disabled = false;}});
$("#isare_emergency_exit_signs_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("are_emergency_exit_signs_presentno").disabled = true;}else{document.getElementById("are_emergency_exit_signs_presentno").disabled = false;}});
$("#isare_emergency_exit_lights_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("are_emergency_exit_lights_presentno").disabled = true;}else{document.getElementById("are_emergency_exit_lights_presentno").disabled = false;}});
$("#isare_emergency_exit_signs_and_lights_operationalno").change(function(){if($(this).is(":checked")){ document.getElementById("are_emergency_exit_signs_and_lights_operationalno").disabled = true;}else{document.getElementById("are_emergency_exit_signs_and_lights_operationalno").disabled = false;}});
$("#isare_corridors_free_of_obstructionsno").change(function(){if($(this).is(":checked")){ document.getElementById("are_corridors_free_of_obstructionsno").disabled = true;}else{document.getElementById("are_corridors_free_of_obstructionsno").disabled = false;}});
$("#isis_corridors_width_minimum_of_44_inchesyes").change(function(){if($(this).is(":checked")){ document.getElementById("is_corridors_width_minimum_of_44_inchesyes").disabled = true;}else{document.getElementById("is_corridors_width_minimum_of_44_inchesyes").disabled = false;}});
$("#isif_occupancy_lease_than_50").change(function(){if($(this).is(":checked")){ document.getElementById("if_occupancy_lease_than_50").disabled = true;}else{document.getElementById("if_occupancy_lease_than_50").disabled = false;}});
$("#iscommon_path_of_egress_travel_isless_than_125_ft_with_sprinklers").change(function(){if($(this).is(":checked")){ document.getElementById("common_path_of_egress_travel_isless_than_125_ft_with_sprinklers").disabled = true;}else{document.getElementById("common_path_of_egress_travel_isless_than_125_ft_with_sprinklers").disabled = false;}});
$("#isfire_extinguishers_travel_isless_than_75_feet").change(function(){if($(this).is(":checked")){ document.getElementById("fire_extinguishers_travel_isless_than_75_feet").disabled = true;}else{document.getElementById("fire_extinguishers_travel_isless_than_75_feet").disabled = false;}});
$("#isemergency_exit_doors_equipped_with_panic_hardwareno").change(function(){if($(this).is(":checked")){ document.getElementById("emergency_exit_doors_equipped_with_panic_hardwareno").disabled = true;}else{document.getElementById("emergency_exit_doors_equipped_with_panic_hardwareno").disabled = false;}});
$("#isemergency_exit_doors_clear_on_both_sideno").change(function(){if($(this).is(":checked")){ document.getElementById("emergency_exit_doors_clear_on_both_sideno").disabled = true;}else{document.getElementById("emergency_exit_doors_clear_on_both_sideno").disabled = false;}});
$("#isdo_emergency_exit_doors_open_properlyno").change(function(){if($(this).is(":checked")){ document.getElementById("do_emergency_exit_doors_open_properlyno").disabled = true;}else{document.getElementById("do_emergency_exit_doors_open_properlyno").disabled = false;}});
$("#ishallstair_smoke_doorsfire_rated").change(function(){if($(this).is(":checked")){ document.getElementById("hallstair_smoke_doorsfire_rated").disabled = true;}else{document.getElementById("hallstair_smoke_doorsfire_rated").disabled = false;}});
$("#ishallstair_smoke_door_closersself_closing").change(function(){if($(this).is(":checked")){ document.getElementById("hallstair_smoke_door_closersself_closing").disabled = true;}else{document.getElementById("hallstair_smoke_door_closersself_closing").disabled = false;}});
$("#isunits_present_for_hearing_impaired").change(function(){if($(this).is(":checked")){ document.getElementById("units_present_for_hearing_impaired").disabled = true;}else{document.getElementById("units_present_for_hearing_impaired").disabled = false;}});
$("#isis_smoke_detector_present").change(function(){if($(this).is(":checked")){ document.getElementById("is_smoke_detector_present").disabled = true;}else{document.getElementById("is_smoke_detector_present").disabled = false;}});
$("#isis_co_detector_present").change(function(){if($(this).is(":checked")){ document.getElementById("is_co_detector_present").disabled = true;}else{document.getElementById("is_co_detector_present").disabled = false;}});
$("#isis_fire_alarmstrobe_light_present").change(function(){if($(this).is(":checked")){ document.getElementById("is_fire_alarmstrobe_light_present").disabled = true;}else{document.getElementById("is_fire_alarmstrobe_light_present").disabled = false;}});
$("#isis_evacuation_plan_posted").change(function(){if($(this).is(":checked")){ document.getElementById("is_evacuation_plan_posted").disabled = true;}else{document.getElementById("is_evacuation_plan_posted").disabled = false;}});
$("#isdoors_closures_present").change(function(){if($(this).is(":checked")){ document.getElementById("doors_closures_present").disabled = true;}else{document.getElementById("doors_closures_present").disabled = false;}});
$("#isare_doors_fire_rated").change(function(){if($(this).is(":checked")){ document.getElementById("are_doors_fire_rated").disabled = true;}else{document.getElementById("are_doors_fire_rated").disabled = false;}});
$("#isfire_door_ul_ratings").change(function(){if($(this).is(":checked")){ document.getElementById("fire_door_ul_ratings").disabled = true;}else{document.getElementById("fire_door_ul_ratings").disabled = false;}});
$("#isdoor_locks_secure").change(function(){if($(this).is(":checked")){ document.getElementById("door_locks_secure").disabled = true;}else{document.getElementById("door_locks_secure").disabled = false;}});
$("#iscooking_appliances_present").change(function(){if($(this).is(":checked")){ document.getElementById("cooking_appliances_present").disabled = true;}else{document.getElementById("cooking_appliances_present").disabled = false;}});
$("#isare_refrigerators_present").change(function(){if($(this).is(":checked")){ document.getElementById("are_refrigerators_present").disabled = true;}else{document.getElementById("are_refrigerators_present").disabled = false;}});
$("#isare_food_sources_present").change(function(){if($(this).is(":checked")){ document.getElementById("are_food_sources_present").disabled = true;}else{document.getElementById("are_food_sources_present").disabled = false;}});
$("#islofts_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("lofts_presentno").disabled = true;}else{document.getElementById("lofts_presentno").disabled = false;}});
$("#isconstruction_soundna").change(function(){if($(this).is(":checked")){ document.getElementById("construction_soundna").disabled = true;}else{document.getElementById("construction_soundna").disabled = false;}});
$("#issprinkler_heads_obstructedna").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_heads_obstructedna").disabled = true;}else{document.getElementById("sprinkler_heads_obstructedna").disabled = false;}});
$("#isadequate_ladder_for_egressna").change(function(){if($(this).is(":checked")){ document.getElementById("adequate_ladder_for_egressna").disabled = true;}else{document.getElementById("adequate_ladder_for_egressna").disabled = false;}});
$("#isdoes_loft_obstruct_egressna").change(function(){if($(this).is(":checked")){ document.getElementById("does_loft_obstruct_egressna").disabled = true;}else{document.getElementById("does_loft_obstruct_egressna").disabled = false;}});
$("#isdoesloft_extend_over_openingsna").change(function(){if($(this).is(":checked")){ document.getElementById("doesloft_extend_over_openingsna").disabled = true;}else{document.getElementById("doesloft_extend_over_openingsna").disabled = false;}});
$("#isconstruction_furniture_gradena").change(function(){if($(this).is(":checked")){ document.getElementById("construction_furniture_gradena").disabled = true;}else{document.getElementById("construction_furniture_gradena").disabled = false;}});
$("#isloft_minimum_clearance_to_ceiling_39na").change(function(){if($(this).is(":checked")){ document.getElementById("loft_minimum_clearance_to_ceiling_39na").disabled = true;}else{document.getElementById("loft_minimum_clearance_to_ceiling_39na").disabled = false;}});
$("#isloft_have_privacy_curtainsna").change(function(){if($(this).is(":checked")){ document.getElementById("loft_have_privacy_curtainsna").disabled = true;}else{document.getElementById("loft_have_privacy_curtainsna").disabled = false;}});
$("#iscode_compliant_outletsna").change(function(){if($(this).is(":checked")){ document.getElementById("code_compliant_outletsna").disabled = true;}else{document.getElementById("code_compliant_outletsna").disabled = false;}});
$("#isentrance_doors_locked_at_all_timesno").change(function(){if($(this).is(":checked")){ document.getElementById("entrance_doors_locked_at_all_timesno").disabled = true;}else{document.getElementById("entrance_doors_locked_at_all_timesno").disabled = false;}});
$("#isentrance_doors_have_closersno").change(function(){if($(this).is(":checked")){ document.getElementById("entrance_doors_have_closersno").disabled = true;}else{document.getElementById("entrance_doors_have_closersno").disabled = false;}});
$("#isdoorshardware_in_good_conditionno").change(function(){if($(this).is(":checked")){ document.getElementById("doorshardware_in_good_conditionno").disabled = true;}else{document.getElementById("doorshardware_in_good_conditionno").disabled = false;}});
$("#isany_visible_damage_to_wallsno").change(function(){if($(this).is(":checked")){ document.getElementById("any_visible_damage_to_wallsno").disabled = true;}else{document.getElementById("any_visible_damage_to_wallsno").disabled = false;}});
$("#isany_ceiling_stains_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("any_ceiling_stains_presentno").disabled = true;}else{document.getElementById("any_ceiling_stains_presentno").disabled = false;}});
$("#isare_all_lights_operaationalno").change(function(){if($(this).is(":checked")){ document.getElementById("are_all_lights_operaationalno").disabled = true;}else{document.getElementById("are_all_lights_operaationalno").disabled = false;}});
$("#isbathsshowers_in_good_conditionno").change(function(){if($(this).is(":checked")){ document.getElementById("bathsshowers_in_good_conditionno").disabled = true;}else{document.getElementById("bathsshowers_in_good_conditionno").disabled = false;}});
$("#isexhaust_fans_testedna").change(function(){if($(this).is(":checked")){ document.getElementById("exhaust_fans_testedna").disabled = true;}else{document.getElementById("exhaust_fans_testedna").disabled = false;}});
$("#isis_there_an_elevatorno").change(function(){if($(this).is(":checked")){ document.getElementById("is_there_an_elevatorno").disabled = true;}else{document.getElementById("is_there_an_elevatorno").disabled = false;}});
$("#isare_there_any_fireplaces_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("are_there_any_fireplaces_presentno").disabled = true;}else{document.getElementById("are_there_any_fireplaces_presentno").disabled = false;}});
$("#isbasement_presentno").change(function(){if($(this).is(":checked")){ document.getElementById("basement_presentno").disabled = true;}else{document.getElementById("basement_presentno").disabled = false;}});
$("#isbasement_sump_pumpsno").change(function(){if($(this).is(":checked")){ document.getElementById("basement_sump_pumpsno").disabled = true;}else{document.getElementById("basement_sump_pumpsno").disabled = false;}});
$("#isevidence_of_leaks_odorsna").change(function(){if($(this).is(":checked")){ document.getElementById("evidence_of_leaks_odorsna").disabled = true;}else{document.getElementById("evidence_of_leaks_odorsna").disabled = false;}});
$("#iselectrical_inspected1").change(function(){if($(this).is(":checked")){ document.getElementById("electrical_inspected1").disabled = true;}else{document.getElementById("electrical_inspected1").disabled = false;}});
$("#istype_of_kitchen").change(function(){if($(this).is(":checked")){ document.getElementById("type_of_kitchen").disabled = true;}else{document.getElementById("type_of_kitchen").disabled = false;}});
$("#isfuel_source_for_stove").change(function(){if($(this).is(":checked")){ document.getElementById("fuel_source_for_stove").disabled = true;}else{document.getElementById("fuel_source_for_stove").disabled = false;}});
$("#isexhaust_hood_present").change(function(){if($(this).is(":checked")){ document.getElementById("exhaust_hood_present").disabled = true;}else{document.getElementById("exhaust_hood_present").disabled = false;}});
$("#isgrease_filters_clean").change(function(){if($(this).is(":checked")){ document.getElementById("grease_filters_clean").disabled = true;}else{document.getElementById("grease_filters_clean").disabled = false;}});
$("#iskitchen_fire_suppression_system_present").change(function(){if($(this).is(":checked")){ document.getElementById("kitchen_fire_suppression_system_present").disabled = true;}else{document.getElementById("kitchen_fire_suppression_system_present").disabled = false;}});
$("#isexhaust_hood_cleaning").change(function(){if($(this).is(":checked")){ document.getElementById("exhaust_hood_cleaning").disabled = true;}else{document.getElementById("exhaust_hood_cleaning").disabled = false;}});
$("#isare_nozzles_clean").change(function(){if($(this).is(":checked")){ document.getElementById("are_nozzles_clean").disabled = true;}else{document.getElementById("are_nozzles_clean").disabled = false;}});
$("#isare_fire_inspection_tags_current").change(function(){if($(this).is(":checked")){ document.getElementById("are_fire_inspection_tags_current").disabled = true;}else{document.getElementById("are_fire_inspection_tags_current").disabled = false;}});
$("#isis_a_fuel_shutoff_available").change(function(){if($(this).is(":checked")){ document.getElementById("is_a_fuel_shutoff_available").disabled = true;}else{document.getElementById("is_a_fuel_shutoff_available").disabled = false;}});
$("#isare_discharged_extinguishers_present").change(function(){if($(this).is(":checked")){ document.getElementById("are_discharged_extinguishers_present").disabled = true;}else{document.getElementById("are_discharged_extinguishers_present").disabled = false;}});
$("#iskitchen_light_bulb_tubes").change(function(){if($(this).is(":checked")){ document.getElementById("kitchen_light_bulb_tubes").disabled = true;}else{document.getElementById("kitchen_light_bulb_tubes").disabled = false;}});
$("#iselectrical_inspected2").change(function(){if($(this).is(":checked")){ document.getElementById("electrical_inspected2").disabled = true;}else{document.getElementById("electrical_inspected2").disabled = false;}});
$("#iselectrical_power_is").change(function(){if($(this).is(":checked")){ document.getElementById("electrical_power_is").disabled = true;}else{document.getElementById("electrical_power_is").disabled = false;}});
$("#isif_overhead_mast").change(function(){if($(this).is(":checked")){ document.getElementById("if_overhead_mast").disabled = true;}else{document.getElementById("if_overhead_mast").disabled = false;}});
$("#ismeter_box_mounted").change(function(){if($(this).is(":checked")){ document.getElementById("meter_box_mounted").disabled = true;}else{document.getElementById("meter_box_mounted").disabled = false;}});
$("#isexterior_junction_boxes").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_junction_boxes").disabled = true;}else{document.getElementById("exterior_junction_boxes").disabled = false;}});
$("#isexterior_disconnects_are").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_disconnects_are").disabled = true;}else{document.getElementById("exterior_disconnects_are").disabled = false;}});
$("#isexterior_conduit").change(function(){if($(this).is(":checked")){ document.getElementById("exterior_conduit").disabled = true;}else{document.getElementById("exterior_conduit").disabled = false;}});
$("#iselectrical_panels_clear").change(function(){if($(this).is(":checked")){ document.getElementById("electrical_panels_clear").disabled = true;}else{document.getElementById("electrical_panels_clear").disabled = false;}});
$("#iselectrical_panels_marked").change(function(){if($(this).is(":checked")){ document.getElementById("electrical_panels_marked").disabled = true;}else{document.getElementById("electrical_panels_marked").disabled = false;}});
$("#isbathrooms_gfi_outlets").change(function(){if($(this).is(":checked")){ document.getElementById("bathrooms_gfi_outlets").disabled = true;}else{document.getElementById("bathrooms_gfi_outlets").disabled = false;}});
$("#iskitchen_gfi_outlets").change(function(){if($(this).is(":checked")){ document.getElementById("kitchen_gfi_outlets").disabled = true;}else{document.getElementById("kitchen_gfi_outlets").disabled = false;}});
$("#isbedrooms_afci_circuits").change(function(){if($(this).is(":checked")){ document.getElementById("bedrooms_afci_circuits").disabled = true;}else{document.getElementById("bedrooms_afci_circuits").disabled = false;}});
$("#isextension_cords_in_use").change(function(){if($(this).is(":checked")){ document.getElementById("extension_cords_in_use").disabled = true;}else{document.getElementById("extension_cords_in_use").disabled = false;}});
$("#isdevice_power_cords_used").change(function(){if($(this).is(":checked")){ document.getElementById("device_power_cords_used").disabled = true;}else{document.getElementById("device_power_cords_used").disabled = false;}});
$("#isplates_missingdamaged").change(function(){if($(this).is(":checked")){ document.getElementById("plates_missingdamaged").disabled = true;}else{document.getElementById("plates_missingdamaged").disabled = false;}});
$("#isdo_circuits_feel_warm").change(function(){if($(this).is(":checked")){ document.getElementById("do_circuits_feel_warm").disabled = true;}else{document.getElementById("do_circuits_feel_warm").disabled = false;}});
$("#iseletrical_panel_missing_blanks").change(function(){if($(this).is(":checked")){ document.getElementById("eletrical_panel_missing_blanks").disabled = true;}else{document.getElementById("eletrical_panel_missing_blanks").disabled = false;}});
$("#isir_photo_of_panel_taken").change(function(){if($(this).is(":checked")){ document.getElementById("ir_photo_of_panel_taken").disabled = true;}else{document.getElementById("ir_photo_of_panel_taken").disabled = false;}});
$("#istrace_of_dorm_outlets").change(function(){if($(this).is(":checked")){ document.getElementById("trace_of_dorm_outlets").disabled = true;}else{document.getElementById("trace_of_dorm_outlets").disabled = false;}});
$("#iscircuit_load_tested").change(function(){if($(this).is(":checked")){ document.getElementById("circuit_load_tested").disabled = true;}else{document.getElementById("circuit_load_tested").disabled = false;}});
$("#ismechanical_inspected").change(function(){if($(this).is(":checked")){ document.getElementById("mechanical_inspected").disabled = true;}else{document.getElementById("mechanical_inspected").disabled = false;}});
$("#isis_building_air_conditioned").change(function(){if($(this).is(":checked")){ document.getElementById("is_building_air_conditioned").disabled = true;}else{document.getElementById("is_building_air_conditioned").disabled = false;}});
$("#isis_boiler_present").change(function(){if($(this).is(":checked")){ document.getElementById("is_boiler_present").disabled = true;}else{document.getElementById("is_boiler_present").disabled = false;}});
$("#iswhat_is_heat_source").change(function(){if($(this).is(":checked")){ document.getElementById("what_is_heat_source").disabled = true;}else{document.getElementById("what_is_heat_source").disabled = false;}});
$("#isis_smoke_detector_in_return_air").change(function(){if($(this).is(":checked")){ document.getElementById("is_smoke_detector_in_return_air").disabled = true;}else{document.getElementById("is_smoke_detector_in_return_air").disabled = false;}});
$("#isis_smoke_detector_connected_to_a_shut_trip").change(function(){if($(this).is(":checked")){ document.getElementById("is_smoke_detector_connected_to_a_shut_trip").disabled = true;}else{document.getElementById("is_smoke_detector_connected_to_a_shut_trip").disabled = false;}});
$("#istype_of_water_heater").change(function(){if($(this).is(":checked")){ document.getElementById("type_of_water_heater").disabled = true;}else{document.getElementById("type_of_water_heater").disabled = false;}});
$("#iswater_heater_capacity").change(function(){if($(this).is(":checked")){ document.getElementById("water_heater_capacity").disabled = true;}else{document.getElementById("water_heater_capacity").disabled = false;}});
$("#iswater_heater_quantity").change(function(){if($(this).is(":checked")){ document.getElementById("water_heater_quantity").disabled = true;}else{document.getElementById("water_heater_quantity").disabled = false;}});
$("#isdrain_pan_present").change(function(){if($(this).is(":checked")){ document.getElementById("drain_pan_present").disabled = true;}else{document.getElementById("drain_pan_present").disabled = false;}});
$("#isexpansion_tank_present").change(function(){if($(this).is(":checked")){ document.getElementById("expansion_tank_present").disabled = true;}else{document.getElementById("expansion_tank_present").disabled = false;}});
$("#ispressure_relief_valve").change(function(){if($(this).is(":checked")){ document.getElementById("pressure_relief_valve").disabled = true;}else{document.getElementById("pressure_relief_valve").disabled = false;}});
$("#iscondition_of_hwh_tank").change(function(){if($(this).is(":checked")){ document.getElementById("condition_of_hwh_tank").disabled = true;}else{document.getElementById("condition_of_hwh_tank").disabled = false;}});
$("#isroof_inspected").change(function(){if($(this).is(":checked")){ document.getElementById("roof_inspected").disabled = true;}else{document.getElementById("roof_inspected").disabled = false;}});
$("#isis_roof_under_warranty").change(function(){if($(this).is(":checked")){ document.getElementById("is_roof_under_warranty").disabled = true;}else{document.getElementById("is_roof_under_warranty").disabled = false;}});
$("#isroof_leaks_reported_or_visible").change(function(){if($(this).is(":checked")){ document.getElementById("roof_leaks_reported_or_visible").disabled = true;}else{document.getElementById("roof_leaks_reported_or_visible").disabled = false;}});
$("#isis_roof_accessible_from_interior").change(function(){if($(this).is(":checked")){ document.getElementById("is_roof_accessible_from_interior").disabled = true;}else{document.getElementById("is_roof_accessible_from_interior").disabled = false;}});
$("#isis_mechanical_equipment_on_roof").change(function(){if($(this).is(":checked")){ document.getElementById("is_mechanical_equipment_on_roof").disabled = true;}else{document.getElementById("is_mechanical_equipment_on_roof").disabled = false;}});
$("#isis_roof_equipment_maintained").change(function(){if($(this).is(":checked")){ document.getElementById("is_roof_equipment_maintained").disabled = true;}else{document.getElementById("is_roof_equipment_maintained").disabled = false;}});
$("#isroof_penetrations_allowing_water_entry").change(function(){if($(this).is(":checked")){ document.getElementById("roof_penetrations_allowing_water_entry").disabled = true;}else{document.getElementById("roof_penetrations_allowing_water_entry").disabled = false;}});
$("#isare_multiple_roofs_installed").change(function(){if($(this).is(":checked")){ document.getElementById("are_multiple_roofs_installed").disabled = true;}else{document.getElementById("are_multiple_roofs_installed").disabled = false;}});
$("#isare_guttersdownspouts_installed").change(function(){if($(this).is(":checked")){ document.getElementById("are_guttersdownspouts_installed").disabled = true;}else{document.getElementById("are_guttersdownspouts_installed").disabled = false;}});
$("#iscondition_of_guttersdownspouts").change(function(){if($(this).is(":checked")){ document.getElementById("condition_of_guttersdownspouts").disabled = true;}else{document.getElementById("condition_of_guttersdownspouts").disabled = false;}});
$("#isfire_safety_inspected").change(function(){if($(this).is(":checked")){ document.getElementById("fire_safety_inspected").disabled = true;}else{document.getElementById("fire_safety_inspected").disabled = false;}});
$("#issupervisory_signals").change(function(){if($(this).is(":checked")){ document.getElementById("supervisory_signals").disabled = true;}else{document.getElementById("supervisory_signals").disabled = false;}});
$("#istamper_switches_and_flow_switch_devices__test_semiannually").change(function(){if($(this).is(":checked")){ document.getElementById("tamper_switches_and_flow_switch_devices__test_semiannually").disabled = true;}else{document.getElementById("tamper_switches_and_flow_switch_devices__test_semiannually").disabled = false;}});
$("#isduct_heat_smoke_detectors_pull_boxes_electrical_releasing_devices__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("duct_heat_smoke_detectors_pull_boxes_electrical_releasing_devices__test_annually").disabled = true;}else{document.getElementById("duct_heat_smoke_detectors_pull_boxes_electrical_releasing_devices__test_annually").disabled = false;}});
$("#isnotification_devices_audible_and_visual__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("notification_devices_audible_and_visual__test_annually").disabled = true;}else{document.getElementById("notification_devices_audible_and_visual__test_annually").disabled = false;}});
$("#isemergency_services_notification_transmission_equipment__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("emergency_services_notification_transmission_equipment__test_annually").disabled = true;}else{document.getElementById("emergency_services_notification_transmission_equipment__test_annually").disabled = false;}});
$("#isfire_pumps_tested__weekly").change(function(){if($(this).is(":checked")){ document.getElementById("fire_pumps_tested__weekly").disabled = true;}else{document.getElementById("fire_pumps_tested__weekly").disabled = false;}});
$("#iswater_storage_tank_highlow_level_alarms__tested_semiannually").change(function(){if($(this).is(":checked")){ document.getElementById("water_storage_tank_highlow_level_alarms__tested_semiannually").disabled = true;}else{document.getElementById("water_storage_tank_highlow_level_alarms__tested_semiannually").disabled = false;}});
$("#iswater_storage_tank_low_water_temperature_alarms_cold_weather_only__test_monthly").change(function(){if($(this).is(":checked")){ document.getElementById("water_storage_tank_low_water_temperature_alarms_cold_weather_only__test_monthly").disabled = true;}else{document.getElementById("water_storage_tank_low_water_temperature_alarms_cold_weather_only__test_monthly").disabled = false;}});
$("#issprinkler_system_main_drain_tests_on_all_risers__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("sprinkler_system_main_drain_tests_on_all_risers__test_annually").disabled = true;}else{document.getElementById("sprinkler_system_main_drain_tests_on_all_risers__test_annually").disabled = false;}});
$("#isfire_deparment_connections_inspected_fire_hose_connections_na__tested_quarterly").change(function(){if($(this).is(":checked")){ document.getElementById("fire_deparment_connections_inspected_fire_hose_connections_na__tested_quarterly").disabled = true;}else{document.getElementById("fire_deparment_connections_inspected_fire_hose_connections_na__tested_quarterly").disabled = false;}});
$("#isfire_pumps_tested_annually_underflow__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("fire_pumps_tested_annually_underflow__test_annually").disabled = true;}else{document.getElementById("fire_pumps_tested_annually_underflow__test_annually").disabled = false;}});
$("#isstandpipe_flow_test_every_5_years__test_every_5_years").change(function(){if($(this).is(":checked")){ document.getElementById("standpipe_flow_test_every_5_years__test_every_5_years").disabled = true;}else{document.getElementById("standpipe_flow_test_every_5_years__test_every_5_years").disabled = false;}});
$("#iskitchen_suppression_semi_annual_testing__test_semiannually").change(function(){if($(this).is(":checked")){ document.getElementById("kitchen_suppression_semi_annual_testing__test_semiannually").disabled = true;}else{document.getElementById("kitchen_suppression_semi_annual_testing__test_semiannually").disabled = false;}});
$("#isgaseous_extinguishing_system_inspected_no_disharge_required__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("gaseous_extinguishing_system_inspected_no_disharge_required__test_annually").disabled = true;}else{document.getElementById("gaseous_extinguishing_system_inspected_no_disharge_required__test_annually").disabled = false;}});
$("#isportable_fire_exinguisher_monthly__test_monthly").change(function(){if($(this).is(":checked")){ document.getElementById("portable_fire_exinguisher_monthly__test_monthly").disabled = true;}else{document.getElementById("portable_fire_exinguisher_monthly__test_monthly").disabled = false;}});
$("#isportable_fire_exinguisher_maintained_annually_0_test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("portable_fire_exinguisher_maintained_annually_0_test_annually").disabled = true;}else{document.getElementById("portable_fire_exinguisher_maintained_annually_0_test_annually").disabled = false;}});
$("#isfire_hoses_hydrostatic_tested_5_years_after_install__every_3_years_after").change(function(){if($(this).is(":checked")){ document.getElementById("fire_hoses_hydrostatic_tested_5_years_after_install__every_3_years_after").disabled = true;}else{document.getElementById("fire_hoses_hydrostatic_tested_5_years_after_install__every_3_years_after").disabled = false;}});
$("#issmoke_detection_shutdown_devices_for_hvac_equipment___test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("smoke_detection_shutdown_devices_for_hvac_equipment___test_annually").disabled = true;}else{document.getElementById("smoke_detection_shutdown_devices_for_hvac_equipment___test_annually").disabled = false;}});
$("#isall_horizontalvertical_roller_and_slider_doors__test_annually").change(function(){if($(this).is(":checked")){ document.getElementById("all_horizontalvertical_roller_and_slider_doors__test_annually").disabled = true;}else{document.getElementById("all_horizontalvertical_roller_and_slider_doors__test_annually").disabled = false;}});
$("#isall_tranfer_switches_12_times_per_year_not_20_or_40_days_apart__test_monthly").change(function(){if($(this).is(":checked")){ document.getElementById("all_tranfer_switches_12_times_per_year_not_20_or_40_days_apart__test_monthly").disabled = true;}else{document.getElementById("all_tranfer_switches_12_times_per_year_not_20_or_40_days_apart__test_monthly").disabled = false;}});
$("#isgenerator_load_test_once_every_36_months_for_4_hours__tested_every_36_months").change(function(){if($(this).is(":checked")){ document.getElementById("generator_load_test_once_every_36_months_for_4_hours__tested_every_36_months").disabled = true;}else{document.getElementById("generator_load_test_once_every_36_months_for_4_hours__tested_every_36_months").disabled = false;}});
$("#isquarterly_functional_test_of_ep_for_5_minclass60_full_duration_of_class_annual").change(function(){if($(this).is(":checked")){ document.getElementById("quarterly_functional_test_of_ep_for_5_minclass60_full_duration_of_class_annual").disabled = true;}else{document.getElementById("quarterly_functional_test_of_ep_for_5_minclass60_full_duration_of_class_annual").disabled = false;}});
$("#isif_generator_testing_fails_review_contingencyimplement_protective_measures__per_occurrence").change(function(){if($(this).is(":checked")){ document.getElementById("if_generator_testing_fails_review_contingencyimplement_protective_measures__per_occurrence").disabled = true;}else{document.getElementById("if_generator_testing_fails_review_contingencyimplement_protective_measures__per_occurrence").disabled = false;}});
$("#isif_load_test_fails_retest_after_repairs__per_occurrence").change(function(){if($(this).is(":checked")){ document.getElementById("if_load_test_fails_retest_after_repairs__per_occurrence").disabled = true;}else{document.getElementById("if_load_test_fails_retest_after_repairs__per_occurrence").disabled = false;}});







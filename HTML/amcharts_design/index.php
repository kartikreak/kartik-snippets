
<link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      crossorigin="anonymous"
    />


<script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
      integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
      crossorigin="anonymous"
    ></script>
    <script src="//www.amcharts.com/lib/4/core.js"></script>
    <script src="//www.amcharts.com/lib/4/charts.js"></script>
    <script src="//www.amcharts.com/lib/4/maps.js"></script>
    <script src="mercycorps-theme.js"></script>
    <script src="https://www.amcharts.com/lib/4/geodata/continentsLow.js"></script>
    <script src="https://www.amcharts.com/lib/4/geodata/worldLow.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script src="spectrumChart.js"></script>


<style>
    .chart4{
        height:500px;
        width:100%;
    }
</style>



<script>
    // chart 4
    am4core.ready(function() {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart4", am4charts.XYChart);

        // Export
        chart.exporting.menu = new am4core.ExportMenu();

        // Data for both series
        var data = [
          {
            year: "2013 Q1",            
            income: 23.5,
            income1: 24.5,
            expenses: 21.1
          },
          {
            year: "2013 Q2",            
            income: 26.2,
            income1: 24.5,
            expenses: 30.5
          },
          {
            year: "2013 Q3",            
            income: 30.1,
            income1: 24.5,
            expenses: 34.9
          },
          {
            year: "2013 Q4",            
            income: 29.5,
            income1: 24.5,
            expenses: 31.1
          },
          {
            year: "2014 Q1",            
            income: 30.6,
            income1: 24.5,
            expenses: 28.2,            
          },
          {
            year: "2014 Q2",            
            income: 34.1,
            income1: 24.5,
            expenses: 32.9,
            
          }
        ];

        /* Create axes */
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";                

        /* Create value axis */
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Removing Grids
        categoryAxis.renderer.grid.template.disabled = true;
        // valueAxis.renderer.grid.template.disabled = true;


        /* Create series 1*/
        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
        columnSeries.name = "Income";
        columnSeries.dataFields.valueY = "income";
        columnSeries.dataFields.categoryX = "year";

        columnSeries.columns.template.tooltipText =
          "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
        columnSeries.columns.template.propertyFields.fillOpacity =
          "fillOpacity";
        columnSeries.columns.template.propertyFields.stroke = "stroke";
        columnSeries.columns.template.propertyFields.strokeWidth =
          "5";
        columnSeries.columns.template.propertyFields.strokeDasharray =
          "columnDash";
        columnSeries.tooltip.label.textAlign = "middle";
        columnSeries.paddingRight = 10;
        

        /////////////////start column series 2
        
        var columnSeries2 = chart.series.push(new am4charts.ColumnSeries());
        columnSeries2.name = "Income";
        columnSeries2.dataFields.valueY = "income";
        columnSeries2.dataFields.categoryX = "year";

        columnSeries2.columns.template.tooltipText =
          "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
        columnSeries2.columns.template.propertyFields.fillOpacity =
          "fillOpacity";
        columnSeries2.columns.template.propertyFields.stroke = "stroke";
        columnSeries2.columns.template.propertyFields.strokeWidth =
          "strokeWidth";
        columnSeries2.columns.template.propertyFields.strokeDasharray =
          "columnDash";
        columnSeries2.tooltip.label.textAlign = "middle";

        /////////////////end column series 2

        /////////////////start column series 3
        
        var columnSeries3 = chart.series.push(new am4charts.ColumnSeries());
        columnSeries3.name = "Income";
        columnSeries3.dataFields.valueY = "income";
        columnSeries3.dataFields.categoryX = "year";

        columnSeries3.columns.template.tooltipText =
          "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
        columnSeries3.columns.template.propertyFields.fillOpacity =
          "fillOpacity";
        columnSeries3.columns.template.propertyFields.stroke = "stroke";
        columnSeries3.columns.template.propertyFields.strokeWidth =
          "strokeWidth";
        columnSeries3.columns.template.propertyFields.strokeDasharray =
          "columnDash";
        columnSeries3.tooltip.label.textAlign = "middle";

        /////////////////end column series 3

        var lineSeries = chart.series.push(new am4charts.LineSeries());
        lineSeries.name = "Expenses";
        lineSeries.dataFields.valueY = "expenses";
        lineSeries.dataFields.categoryX = "year";

        lineSeries.stroke = am4core.color("#fdd400");
        lineSeries.strokeWidth = 3;
        lineSeries.propertyFields.strokeDasharray = "lineDash";
        lineSeries.tooltip.label.textAlign = "middle";

        var bullet = lineSeries.bullets.push(new am4charts.Bullet());
        bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
        bullet.tooltipText =
          "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
        var circle = bullet.createChild(am4core.Circle);
        circle.radius = 4;
        circle.fill = am4core.color("#fff");
        circle.strokeWidth = 3;

        ////////////////////start demo 

        var lineSeries2 = chart.series.push(new am4charts.LineSeries());
        lineSeries2.name = "Income";
        lineSeries2.dataFields.valueY = "income";
        lineSeries2.dataFields.categoryX = "year";

        lineSeries2.stroke = am4core.color("#000");
        lineSeries2.strokeWidth = 3;
        lineSeries2.propertyFields.strokeDasharray = "lineDash";
        lineSeries2.tooltip.label.textAlign = "middle";

        var bullet = lineSeries2.bullets.push(new am4charts.Bullet());
        bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
        bullet.tooltipText =
          "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
        var circle = bullet.createChild(am4core.Circle);
        circle.radius = 4;
        circle.fill = am4core.color("#fff");
        circle.strokeWidth = 3;





        ////////////////////end demo

        chart.data = data;
      }); // end am4core.ready()
      // end of chart4

</script>

<div id="chart4"></div>

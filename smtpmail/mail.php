<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'reakkartik@gmail.com';                 // SMTP username
    $mail->Password = 'reak@2018';                           // SMTP password
    $mail->SMTPSecure = '465';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 25;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('kartik.swamy@reak.in', 'EmailAutomation');
    $mail->addAddress('kartik.swamy@reak.in');     // Add a recipient

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Here is the subject';

    $html = '<div class="gjs-row">
    <div class="gjs-cell">
      <div id="ibvhys">Hello World
        <br id="ipdsmg"/>
      </div>
    </div>
  </div>';
  
  $css = '<style>
  body {
    margin: 0;
  }
  .gjs-row{
    display:table;
    padding-top:10px;
    padding-right:10px;
    padding-bottom:10px;
    padding-left:10px;
    width:100%;
  }
  .gjs-cell{
    width:8%;
    display:table-cell;
    height:75px;
  }
  #ibvhys{
    padding:10px;
    text-align:center;
    font-size:65px;
    color:#922a2a;
  }
  @media (max-width: 768px){
    .gjs-cell{
      width:100%;
      display:block;
    }
    .gjs-cell{
      width:100%;
      display:block;
    }
  }
  </style>
  ';

    $mail->Body    = '<html><head><meta http-equiv="Content-Type" content="text/html charset=UTF-8" />'.$css.'</head><body>'.$html.'</body></html>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
    echo "<hr>";
    echo '<xmp><html><head><meta http-equiv="Content-Type" content="text/html charset=UTF-8" />'.$css.'</head><body>'.$html.'</body></html></xmp>';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
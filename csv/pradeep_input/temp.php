<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public function import_csvAction()
    {
        
        $access = $this->session->get("access");
        if(($access=="admin" || "user")) 
        {
        $this->view->disable();
        $request = new Phalcon\Http\Request();
        
        $this->view->menu_reak = "directory";
        
        $file = fopen("input_csv.csv","r");

        $result = array();

        while(! feof($file))
          {
          $data = fgetcsv($file);
          
          
          
          $name['name'] = $this->name($data[0]);  
          
          
          $contact['contact'] = $this->contact($data[11], $data[12], $data[13]);  
          array_push($name ,$contact);
          
          $email['email'] = $this->insert__single_data($data[14]);  
          array_push($name ,$email);
          
          $group['group'] = $this->insert__single_data($data[1]);  
          array_push($name ,$group);
          
          $division['division'] = $this->insert__single_data($data[2]);  
          array_push($name ,$division);
          
          $sub_division['sub_division'] = $this->insert__single_data($data[3]);  
          array_push($name ,$sub_division);
          
          $organization['organization'] = $this->insert__single_data($data[4]);  
          array_push($name ,$organization);
          
          $design['design'] = $this->insert__single_data($data[5]);  
          array_push($name ,$design);
          
          $title['title'] = $this->insert__single_data($data[10]);  
          array_push($name ,$title);

          
          //array_push($result ,$data[11], $data[12], $data[13], $data[14], $data[1], $data[2], $data[3], $data[4], $data[5], $data[10]);

          //echo $data[6];
           //break;
          }

        fclose($file);
        
        array_push($result ,$name);
        
        //print_r($result);

        $this->upload_data($result);  

        }
        else 
        {
            $this->response->redirect($this->view->appurl.'index/login');
        }
        
    }
    
    private function name($value) 
    {
            $res = explode(";",$value);
            return $res;
    }
    
    private function contact($value1, $value2, $value3) 
    {       
            $res = array($value1, $value2, $value3);            
            return $res;
    }
    
    private function insert__single_data($value) 
    {
            $res = array($value);       
            return $res;
    }
    
    private function upload_data($value) 
    {
            $request = new Phalcon\Http\Request();
                                 
            
            print_r($value);
            
            foreach ($value as $key => $data) 
            {
                //print_r($data);
                
                
             exit;   
            $directory = new DirectoryEntry;        
            $directory->first_name = $data['name'][0];
            $directory->last_name = $data['name'][1];
            $directory->office_phone = $data[0]['contact'][0];
            $directory->mobile_phone = $data[0]['contact'][1];
            $directory->other_contact = $data[0]['contact'][2];
            $directory->email_address = $data[1]['email'][0];           
            $directory->group_name = $data[2]['group'][0];
            $directory->division_name = $data[3]['division'][0];
            $directory->sub_division_name = $data[4]['sub_division'][0];
            $directory->organization_role = $data[5]['organization'][0];
            $directory->design_project = $data[6]['design'][0];
            $directory->title = $data[7]['title'][0];
            $directory->timestamp = strtotime("now");
            
            if ($directory->create()) 
            {
                $this->flash->success("Directory Created Successfully !");
            }
            else
            {
                $this->flash->danger("Directory Not Submitted");
            }
             
            }
            ?>
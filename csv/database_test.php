<?php
$databaseName = '<database name>';

$pdo = new Pdo('mysql:host=<host>;dbname=' . $databaseName, '<user>', '<password>');

$result = $pdo->query('SHOW TABLES FROM ' . $databaseName)->fetchAll(PDO::FETCH_NUM);

$tables = [];
foreach ($result as $r) {
    $tables[] = $r[0];
}

$data = [];
foreach ($tables as $table) {
    $data[$table] = $pdo->query('SELECT * FROM ' . $table)->fetchAll(PDO::FETCH_ASSOC);
}

var_dump($tables);
var_dump($data);
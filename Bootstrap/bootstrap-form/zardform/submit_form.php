<?php

require('db.php');

// define variables and set to empty values
$electrical_power = $overhead_mast = $meter_box_mounted = $exterior_junction_box = $comment_electrical = $address_visible = $floor_height = $plan_available = $alarm_panel = $alarm_panel_monitor = $fire_extinguisher ="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $electrical_power = clear_data($_POST["electrical_power"]);
  $overhead_mast = clear_data($_POST["overhead_mast"]);
  $comment_electrical = clear_data($_POST["comment_electrical"]);
  $exterior_junction_box = clear_data($_POST["exterior_junction_box"]);
  $meter_box_mounted = clear_data($_POST["meter_box_mounted"]);
  $address_visible = clear_data($_POST["address_visible"]);
  $floor_height = clear_data($_POST["floor_height"]);
  $plan_available = clear_data($_POST["plan_available"]);
  $alarm_panel = clear_data($_POST["alarm_panel"]);
  $alarm_panel_monitor = clear_data($_POST["alarm_panel_monitor"]);
  $fire_extinguisher = clear_data($_POST["fire_extinguisher"]);
}

function clear_data($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

    $conn = database_con();
    
    $stmt = $conn->prepare("INSERT INTO inspection_form (electrical_power, overhead_mast, comment_electrical, exterior_junction_box, meter_box_mounted, address_visible, floor_height, plan_available, alarm_panel, alarm_panel_monitor, fire_extinguisher) VALUES (?, ?, ?, ? ,? ,? ,? ,? ,? ,? ,?)");
    $stmt->bind_param("sssssssssss", $electrical_power, $overhead_mast, $comment_electrical, $exterior_junction_box, $meter_box_mounted, $address_visible, $floor_height, $plan_available, $alarm_panel, $alarm_panel_monitor, $fire_extinguisher);
    $stmt->execute();
?>
<h1 class="" data-css="tve-u-15e0146bead" style="text-align: center;">Points Deals&nbsp;</h1>
<p data-css="tve-u-1624e39a727">Here are the most current deals to earn points:</p>
<h2>New York to Tokyo, Japan for only $452 roundtrip</h2><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-2527" alt="" width="640" height="480" title="aircraft-jet-landing-cloud-46148" data-id="2527" src="//www.wiseflys.com/wp-content/uploads/2018/03/aircraft-jet-landing-cloud-46148.jpeg" style="width: 100%;"></span>
<span
    class="tve_not_editable tve_toggle"></span>
    <h4 class="tve_editable">Click Here For Deal</h4>
    <p><strong>DEPART:</strong><strong>New York, USA</strong></p>
    <p><strong>ARRIVE:</strong><strong><strong>Tokyo, Japan</strong></strong>
    </p>
    <p><strong>RETURN:</strong><strong>New York, USA</strong></p>
    <p><strong>DATES:</strong>Limited availability in&nbsp;<strong>October 2018</strong></p>
    <p>Example dates:<a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=nrt&amp;SDP0=12-10-2018&amp;SO1=nrt&amp;SD1=nyc&amp;SDP1=30-10-2018&amp;AD=1&amp;TK=ECO&amp;DO=False&amp;NA=false&amp;currency=USD&amp;plr=false"
            rel="noopener" target="_blank">12th-30th Oct</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=nrt&amp;SDP0=17-10-2018&amp;SO1=nrt&amp;SD1=nyc&amp;SDP1=31-10-2018&amp;AD=1&amp;TK=ECO&amp;DO=False&amp;NA=false&amp;currency=USD&amp;plr=false"
            rel="noopener" target="_blank">17th-31st Oct</a></p>
    <p><strong>possibly more…</strong></p>
    <p><strong>STOPS:Shanghai</strong></p>
    <p><strong>AIRLINES:</strong><strong>China Eastern Airlines</strong></p>
    <h2>Non-stop from New York to Mexico City, Mexico for only $224 roundtrip</h2><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-2527" alt="" width="640" height="480" title="aircraft-jet-landing-cloud-46148" data-id="2527" src="//www.wiseflys.com/wp-content/uploads/2018/03/aircraft-jet-landing-cloud-46148.jpeg" style="width: 100%;"></span>
    <span
        class="tve_not_editable tve_toggle"></span>
        <h4 class="tve_editable">Click Here For Deal</h4>
        <p>Non-stop flights from New York to Mexico City, Mexico for only $224 roundtrip&nbsp;with Aeromexico.</p>
        <p><strong>DEPART:</strong><strong>New York, USA</strong></p>
        <p><strong>ARRIVE:</strong><strong><strong>Mexico City, Mexico</strong></strong>
        </p>
        <p><strong>RETURN:</strong><strong>New York, USA</strong></p>
        <p><strong>DATES:</strong>Availability from&nbsp;<b>April to May 2018</b></p>
        <p>Example dates:<a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=20-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=28-04-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">20th-28th Apr</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=22-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=04-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">22nd Apr – 4th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=22-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=05-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">22nd Apr – 5th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=24-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=02-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">24th Apr – 2nd May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=24-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=03-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">24th Apr – 3rd May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=24-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=04-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">24th Apr – 4th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=24-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=07-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">24th Apr – 7th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=28-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=09-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">28th Apr – 9th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=30-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=10-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">30th Apr – 10th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=30-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=11-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">30th Apr – 11th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=30-04-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=13-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">30th Apr – 13th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=04-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=11-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">4th-11th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=04-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=13-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">4th-13th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=04-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=14-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">4th-14th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=05-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=16-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">5th-16th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=05-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=17-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">5th-17th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=05-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=18-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">5th-18th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=05-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=19-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">5th-19th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=06-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=13-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">6th-13th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=06-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=14-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">6th-14th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=07-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=16-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">7th-16th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=10-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=19-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">10th-19th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=12-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=22-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">12th-22nd May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=12-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=23-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">12th-23rd May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=17-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=24-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">17th-24th May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=20-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=31-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">20th-31st May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=22-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=31-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">22nd-31st May</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=23-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=30-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
                rel="noopener" target="_blank">23rd-30th May</a></p>
        <p><strong>and more…</strong></p>
        <p>STOPS:</p>
        <p><strong>AIRLINES:</strong><strong>Aeromexico</strong></p><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=4&amp;SegNo=2&amp;SO0=nyc&amp;SD0=mex&amp;SDP0=23-05-2018&amp;SO1=mex&amp;SD1=nyc&amp;SDP1=30-05-2018&amp;AD=1&amp;TK=ECO&amp;DO=True&amp;NA=false&amp;currency=USD&amp;plr=false"
            icon="" target="_blank">GO TO DEAL</a>
        <h2>Non-stop from New York to London or Birmingham, UK from only $99 one-way</h2><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-2527" alt="" width="640" height="480" title="aircraft-jet-landing-cloud-46148" data-id="2527" src="//www.wiseflys.com/wp-content/uploads/2018/03/aircraft-jet-landing-cloud-46148.jpeg" style="width: 100%;"></span>
        <span
            class="tve_not_editable tve_toggle"></span>
            <h4 class="tve_editable">Click Here For Deal</h4>
            <p>Non-stop flights&nbsp;from New York to London or Birmingham, UK from only $99 one-way.</p>
            <p><em>Fly to:</em><em>London: $99</em><em>Birmingham: $149</em></p>
            <p><strong>DEPART:</strong><strong>New York, USA</strong><strong></strong></p>
            <p><strong>ARRIVE:</strong><strong>London/</strong><strong>Birmingham, UK</strong></p>
            <p><strong>DATES:</strong>Availability from&nbsp;<strong>May to December 2018&nbsp;</strong>(excluding summer)</p>
            <p><em>The Primera Air website conveniently allows you to view prices for the whole week so there is no need for a list of example dates from us.However, do NOT purchase on the Primera Air&nbsp;website. Simply find the cheap dates, then purchase on the Momondo GO TO DEAL button below.Momondo OTAs price these flights cheaper.</em></p>
            <p><strong>STOPS:</strong></p>
            <p><strong>AIRLINES:</strong><strong>Primera Air</strong></p><a href="https://primeraair.com/calendar/1/0/0/EWR/2018/06/14/STN/" icon="" target="_blank">FIND DATES FIRST</a><a href="http://track.webgains.com/click.html?wgcampaignid=181273&amp;wgprogramid=8613&amp;wgtarget=http://www.momondo.com/flightsearch/?Search=true&amp;TripType=1&amp;SegNo=1&amp;SO0=nyc&amp;SD0=lon&amp;SDP0=04-06-2018&amp;AD=1&amp;TK=ECO&amp;DO=False&amp;NA=false&amp;currency=USD&amp;plr=false"
                icon="" target="_blank">GO TO DEAL</a>
            <p>Deals courtesy of: Expedia, kayak, secret flying, deals, .....</p>info-circle
            <p data-css="tve-u-15df6ff0812"><strong><span style="color: rgb(64, 140, 96); font-size: 18px;">​</span></strong><span style="font-size: 18px;"><strong>Note:</strong> If you enjoy our products, make sure to check out our premium offer</span></p><a href="#" class="tcb-button-link"
                data-css="tve-u-15dd32e9492"><span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-15df701aedf"><span style="font-family: &quot;Open Sans&quot;; font-weight: 400; font-size: 14px;"><strong class="">CHECK IT NOW</strong></span></span></span></a>
            <p
                data-css="tve-u-15dd3850add"><span style="color: rgb(110, 117, 124); font-family: &quot;Open Sans&quot;; font-weight: 300;">© {tcb_current_year} Thrive Landing Pages. All rights Reserved</span></p>
                <p data-css="tve-u-15dd3850add" style="text-align: right;"><a href="https://thrivethemes.com/?tcb_preview=edition-author-lead-generation#"><span style="color: rgb(110, 117, 124); font-family: &quot;Open Sans&quot;; font-weight: 300;"><u>Disclaimer</u></span></a></p>
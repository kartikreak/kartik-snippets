<?php

class FakeModel {
    public $integer = "123";
    public $string = "123";
    public $float = "123.32";
}


function createDB(){
    $db = array(
    // Integer strings - Padding 3 zeros
    array("integer", "N", 3, 0),
    // 50 is the lengthj
    array("string", "C", 50),
    array("float", "F", 3, 0),
    );
    if (!dbase_create('tmp/test.dbf', $db)) {
    echo "Error, can't create the database\n";
    }
}

function insertToDB(){
    // 2 is for read - write mode
    $db = dbase_open("tmp/test.dbf", 2);
    $record = array(
    "abc",
    "abc",
    "xyzabcqewqeqwdasdasdsadsdsadasdas"
    );
    dbase_add_record($db, $record);
    dbase_close($db);
}

//createDB();
insertToDB();

$obj = new FakeModel;
print_r($obj->integer);



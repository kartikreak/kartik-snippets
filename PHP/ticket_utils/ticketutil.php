<?php
getInvoicesItems();

function getPO()
{
 
$signature = base64_encode(hash_hmac('sha256', '/POS/PurchaseOrders/Search', 'Salem', true));     

$curl = curl_init();

curl_setopt_array($curl, array(
 CURLOPT_URL => "https://api.ticketutils.com/POS/PurchaseOrders/Search",
 CURLOPT_RETURNTRANSFER => true,
 CURLOPT_ENCODING => "",
 CURLOPT_MAXREDIRS => 10,
 CURLOPT_TIMEOUT => 30,
 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 CURLOPT_CUSTOMREQUEST => "POST",
 CURLOPT_POSTFIELDS => "{}",
 CURLOPT_HTTPHEADER => array(
   "cache-control: no-cache",
   "content-type: application/json",
   "postman-token: 6ad3b4c3-d231-5b8e-126a-23c76e1fa008",
   "x-api-version: 3",
   "x-signature: ".$signature,
   "x-token: 5563030361547534733"
 ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
 echo "cURL Error #:" . $err;
} else {
 echo $response;
}
}


function getSales() 
{
    $signature = base64_encode(hash_hmac('sha256', '/POS/v3.2/Tickets/Search', 'Salem', true));
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.ticketutils.com/POS/v3.2/Tickets/Search",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{}",
    CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 2b821f5c-7496-0e56-754b-0bc78282851a",
    "x-api-version: 3",
    "x-signature: ".$signature,
    "x-token: 5563030361547534733"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

curl_close($curl);

if ($err) {
 echo "cURL Error #:" . $err;
} else {
 echo $response;
}
}


function getInvoices() {
    $signature = base64_encode(hash_hmac('sha256', '/POS/Sales/Invoices/Search', 'Salem', true));
    $curl = curl_init();

curl_setopt_array($curl, array(
 CURLOPT_URL => "https://api.ticketutils.com/POS/Sales/Invoices/Search",
 CURLOPT_RETURNTRANSFER => true,
 CURLOPT_ENCODING => "",
 CURLOPT_MAXREDIRS => 10,
 CURLOPT_TIMEOUT => 30,
 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 CURLOPT_CUSTOMREQUEST => "POST",
 CURLOPT_POSTFIELDS => "{}",
 CURLOPT_HTTPHEADER => array(
   "cache-control: no-cache",
   "content-type: application/json",
   "postman-token: 2830e683-5367-3215-6aa8-d0d9a4af6697",
   "x-api-version: 3",
   "x-signature: ".$signature,
   "x-token: 5563030361547534733"
 ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
 echo "cURL Error #:" . $err;
} else {
 echo $response;
}
}

function getInvoicesItems() {
  $signature = base64_encode(hash_hmac('sha256', '/POS/Sales/InvoiceItems/Search', 'Salem', true));
  $curl = curl_init();

curl_setopt_array($curl, array(
 CURLOPT_URL => "https://api.ticketutils.com/POS/Sales/InvoiceItems/Search",
 CURLOPT_RETURNTRANSFER => true,
 CURLOPT_ENCODING => "",
 CURLOPT_MAXREDIRS => 10,
 CURLOPT_TIMEOUT => 30,
 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 CURLOPT_CUSTOMREQUEST => "POST",
 CURLOPT_POSTFIELDS => "{}",
 CURLOPT_HTTPHEADER => array(
   "cache-control: no-cache",
   "content-type: application/json",
   "postman-token: d8302aee-9882-a7b6-c0b6-88a6f666b08a",
   "x-api-version: 3",
   "x-signature: ".$signature,
   "x-token: 5563030361547534733"
 ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
 echo "cURL Error #:" . $err;
} else {
 echo $response;
}
}

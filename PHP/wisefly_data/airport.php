<?php
$code = "";
$data = '[{"image":"https://s3.amazonaws.com/ionic-io-static/2sx5eLfyToeBeDOygnuG_Screenshot_2018-10-15%20Hot%20Deal%20Flights%20To%20Perth,%20Australia%20From%20Only%20$599%20Round%20Trip.png", "title":"Price Drop: West Cost To Venice, Italy From $439 Round Trip", "depart":"West Coast, Italy", "arrive":"Venice, Italy", "return":"West Coast, Italy","dates":["01-10 DEC(2018)", "01-10 JAN(2019)", "14-20 APR(2019)", "01-10 MAY(2019)", "11-25 JUN(2019)", "01-17 JUL(2019)", "16-24 AUG(2019)"]}, {"image":"https://s3.amazonaws.com/ionic-io-static/Ev1kgmMCQpOLLvTWLdkL_Screenshot_2018-10-15%207-Night%20Maui%20Vacation%E2%80%94Explore%20Haleakal%C4%81%20National%20Park%20From%20$982%20W%20Flights%20Rental%20Car.png", "title":"Hot Deal! Flight To Perth, Australia From Only $599 Round Trip", "depart":"West Coast, Italy", "arrive":"Venice, Italy", "return":"West Coast, Italy","dates":["01-10 DEC(2018)", "01-10 JAN(2019)", "14-20 APR(2019)", "01-10 MAY(2019)", "11-25 JUN(2019)", "01-17 JUL(2019)", "16-24 AUG(2019)"]}, {"image":"https://s3.amazonaws.com/ionic-io-static/no269MJQoya3BG5ohGYS_Screenshot_2018-10-15%20Price%20Drop%20West%20Coast%20To%20Venice,%20Italy%20From%20$439%20Round%20Trip.png", "title":"Title 3", "depart":"West Coast, Italy", "arrive":"Venice, Italy", "return":"West Coast, Italy","dates":["01-10 DEC(2018)", "01-10 JAN(2019)", "14-20 APR(2019)", "01-10 MAY(2019)", "11-25 JUN(2019)", "01-17 JUL(2019)", "16-24 AUG(2019)"]}, {"image":"https://s3.amazonaws.com/ionic-io-static/XxxLsbhmRuer81vb2HHi_Screenshot_2018-10-15%20Budget%20Vegas%20Vacation%203%20Nights%20From%20$161%20With%20Flights,%20Hotel,%20Resort%20Fees.png", "title":"Title 4"}, {"image":"https://s3.amazonaws.com/ionic-io-static/o9VfOKCtRFyO5tvyXPuP_Phuket-Thailand_196936655.jpg", "title":"Title 5", "depart":"West Coast, Italy", "arrive":"Venice, Italy", "return":"West Coast, Italy","dates":["01-10 DEC(2018)", "01-10 JAN(2019)", "14-20 APR(2019)", "01-10 MAY(2019)", "11-25 JUN(2019)", "01-17 JUL(2019)", "16-24 AUG(2019)"]}]';        
    $code = $_POST["code"];
    $registrationId = $_POST["registrationId"];
    $res = departingAirport($code, $registrationId);    
    if($res == "true")
    {
        echo $data;    
    }
    else
    {
        echo "false";
    }
function departingAirport($code, $registrationId)
{
require './db.php';

$conn = database_con();

// prepare and bind

$stmt = $conn->prepare("SELECT id AS id from departingAirport  where registrationId = ?");
$stmt->bind_param("s", $registrationId);

// set parameters and execute
//
if ($stmt->execute()) {
    // echo $count = count($stmt->get_result());             
    foreach ($stmt->get_result() as $row)
    {
        $count = $row['id'];				
                   
    } 
    if(@$count >= 1)
    {
        // Updating Value 

        $stmt1 = $conn->prepare("UPDATE departingAirport SET code = ? WHERE registrationId = ?");
        $stmt1->bind_param("ss", $code, $registrationId);
        
        // set parameters and execute
        if ($stmt1->execute())
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }
    else
    {   
        // Inserting Value

        $stmt2 = $conn->prepare("INSERT INTO departingAirport (code, registrationId) VALUES (?, ?)");
        $stmt2->bind_param("ss", $code, $registrationId);
        
        // set parameters and execute
        if ($stmt2->execute())
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }  
}

$stmt->close();
$conn->close();
}
?>
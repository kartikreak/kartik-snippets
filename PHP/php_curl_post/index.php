<!DOCTYPE html>
<html>
<style>
table,th,td {
  border : 1px solid black;
  border-collapse: collapse;
}
th,td {
  padding: 5px;
}
</style>
<body>

<button type="button" onclick="loadXMLDoc()">Get my CD collection</button>
<br><br>
<table id="demo"></table>

<script>
deleteProduct(product:Product) {
    if ( this._loggedInGuard.isLoggedIn() ) {
      let token = localStorage.getItem('token');
      let body = JSON.stringify(product);
      let headers = new Headers(
         { 'Content-Type': 'application/json',
            "X-HTTP-Method-Override": "DELETE",
            'Authorization': 'Bearer ' + token});
      return this._http
      .post(this.API + "/products/"+product.id, body, {headers: headers} )
      .map(res => res.json())
      .catch(this._responseService.catchBadResponse)
      .finally(() => this._responseService.success('Well done'));
    }  
  }
</script>



</body>
</html>

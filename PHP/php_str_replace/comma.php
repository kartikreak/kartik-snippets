<?php 
$text = '  


	
	
	
<style type="text/css">p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; }a:link { }</style>


<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.68cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">All
hand tools, power tools and other similar equipment, whether
furnished by the company or the employee, shall be maintained in a
safe condition. Personnel assigned to toot rooms should be
responsible for the inspection and repair of tools under their
control.</font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.18cm" align="left">
<br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<sub><font face="Times New Roman, serif"><font style="font-size: 12pt" size="3"><u>Hand
Tools</u></font></font></sub></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.35cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-top: 0.05cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">a.
Insulated or non-conducting tools should be used when working near
energized electrical circuits,</font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.7cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">b.
Tool handles shall be tightly fitted. Wooden handles should be
carefully checked, tightened with wedges, if necessary, and split or
splintered handles shall be replaced.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">c.
All impact tools, such as chisels, punches and wedges, shall be
regularly dressed to eliminate " mushrooming</font></font></p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<sub><font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">".</font></font></sub></p>
<p style="margin-bottom: 0cm; line-height: 0.21cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-top: 0.05cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3"><u>Power
Tools</u></font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 1.13cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">The
majority of power tool accidents are caused by improper handling or
poor maintenance. The following applies to all types of power tools:</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">a.
Only authorized personnel shall be permitted to operate or repair
power tools</font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.37cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">b.
Maintenance of power tools should be systematic. All worn or damaged
tools should be promptly repaired or replaced. All tools should be
cleaned, tested and inspected regularly.</font></font></p>
<p style="margin-bottom: 0cm; line-height: 0.42cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-right: 1.59cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">c.
Power tools shall not be used if safety devices, such as shields,
tool rests, hoods and guards have been removed or otherwise rendered
inoperative.</font></font></p>
<p style="margin-bottom: 0cm; line-height: 0.42cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.87cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">d.
Employees using tools under conditions that expose them to the
hazards of flying objects or harmful dusts shall be provided with the
required personal protective equipment.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">e.
All electrically powered tools shall be properly grounded, or of
double insulated construction. Outlets used for</font></font></p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">110
volt tools shall be protected by ground fault circuit interruption
devices, or as per the Assured Equipment</font></font></p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">Grounding
Conductor Program.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">f.
Gasoline powered tools shall not be used in unventilated areas.
Gasoline shall be dispensed only in UL</font></font></p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">approved
safety cans.</font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.3cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">g.
Portable grinders should be provided with hood type guards with side
enclosures that cover the spindle and at least 50% of the wheel. All
wheels should be inspected regularly for signs of fracture.</font></font></p>
<p style="margin-bottom: 0cm; line-height: 0.42cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.13cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">h.
Bench grinders shall be equipped with deflector shields and side
cover guards. Tool rests shall have a maximum clearance of 1/8-inch
from the wheel and tongue guards adjusted to a maximum clearance of
1/4-inch.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">i.
Hoses supplying pneumatic tools shall have couplings secured to
prevent accidental disconnections.</font></font></p>
<p style="margin-left: 0.18cm; margin-right: 0.98cm; margin-top: 0.13cm; margin-bottom: 0cm; line-height: 0.46cm; page-break-before: always" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">j.
Air-supply lines should be protected from damage, inspected regularly
and maintained in good condition. Couplings shall be secured by
positive means.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">k.
Air sources supplying hoses exceeding 1/8-inch ID shall be protected
by excess flow valves to prevent</font></font></p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">"whipping"
in the event of hose separation or failure.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">l.
The pressure of compressed air used for cleaning purposes must be
reduced to 30 PSI or less</font></font></p>
<p style="margin-top: 0.01cm; margin-bottom: 0cm; line-height: 0.25cm" align="left">
<br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-bottom: 0cm; line-height: 0.35cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<sub><font face="Times New Roman, serif"><font style="font-size: 12pt" size="3"><u>Powder-Actuated
Tools</u></font></font></sub></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.35cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.12cm; margin-top: 0.07cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">a.
Only employees who have furnished evidence of having been trained in
its use shall be allowed to operate a powder-actuated tool. Eye
protection shall be worn by all personnel exposed to the use of this
type tool. Hearing protection shall also be worn if the tool is used
for extended periods.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">b.
Tools shall not be loaded until just prior to use. Loaded tools shall
not be left unattended.</font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.9cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">c.
Tools shall not be used in an explosive or flammable atmosphere.
Cartridges (power source) shall be kept separated from all other
material.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">d.
Account for all cased power loads. Police area and remove used cases.</font></font></p>
<p style="margin-top: 0.02cm; margin-bottom: 0cm; line-height: 0.39cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-bottom: 0cm; line-height: 100%" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">e.
Powder-actuated tools shall meet all applicable requirements of ANSI
10.3</font></font></p>
<p style="margin-top: 0cm; margin-bottom: 0cm; line-height: 0.42cm" align="left">
<br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.53cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">f.
Fasteners shall not be driven into very hard or brittle materials
including, but not limited to cast iron, glazed tile, surface
hardened steel, glass block, live rock, face brick or hollow tile.</font></font></p>
<p style="margin-bottom: 0cm; line-height: 0.42cm" align="left"><br>

</p>
<p style="margin-left: 0.18cm; margin-right: 0.23cm; margin-bottom: 0cm; line-height: 0.46cm" align="left">
<font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">g.
Driving into materials easily penetrated shall be avoided unless such
materials are backed by a substance that will prevent the pin or
fastener from passing completely through and creating a flying
missile hazard on the other side.</font></font></p>
<p> """"""""" </p>
<p> Hello </p>

';

echo str_replace('"', "'", $text);
?>
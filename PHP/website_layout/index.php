<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Basic Structure</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    
    <?php include 'header.html';?>
    <!-- The flexible grid (content) -->
    <div class="row">
    <?php include 'left_side.html';?>
    <?php include 'main.html';?>    
    </div>    
    <?php include 'footer.html';?> 
</body>

</html>
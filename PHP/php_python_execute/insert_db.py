from subprocess import call
import MySQLdb
import time
import csv, sys, json
import string

'''
Utility Functions

'''




def process(sheet_name, perc):
    	db = MySQLdb.connect("localhost","root","reak","connected_fibre" )
	cursor = db.cursor()
	sql = 'UPDATE `sheet_table` SET `progress`= "'+perc+'" WHERE sheet_name = "'+sheet_name+'"'
	try:
		# Execute the SQL command
		cursor.execute(sql)
		# Commit your changes in the database
		db.commit()
		print("Process Updated")
	except:
		# Rollback in case there is any error
		#db.rollback()
		print("Process Rolling Back DB")
	# disconnect from server
	db.close()
        
def sheet_idfromname(sheet_name):
	
        db = MySQLdb.connect("localhost","root","reak","connected_fibre" )
	cursor = db.cursor()
	sql = ('SELECT id FROM sheet_table WHERE sheet_name = "'+sheet_name+'" ') 
	try:
		# Execute the SQL command
		cursor.execute(sql)
		rows = cursor.fetchone()
                #j = json.dumps(rows,encoding = "ISO-8859-1")
                
		db.commit()
		return rows[0]
                
	except:
		# Rollback in case there is any error
		#db.rollback()		
                return "sheet id not found"
	# disconnect from server
	db.close()
    

def create_smarty_streets(sheet_name):
        
        #path = '/home/reak-1/NetBeansProjects/php_codes/php_python_execute/';
        #call(["smartylist", '-auth-id="3bb0491a-c30e-1c61-5c93-7c856d78de52"', '-auth-token="3yES71p3L97EkQkwJXvj"', '-input='+input_file+sheet_name, '-output='+output_file+'output-'+sheet_name, '-log=log.txt'])
        #call(['smartylist -auth-id="3bb0491a-c30e-1c61-5c93-7c856d78de52" -auth-token="3yES71p3L97EkQkwJXvj"  -input="'+path+sheet_name+'" -output="'+path+'output-'+sheet_name+'" -log="log.txt"'])
        call(['smartylist', '-auth-id="3bb0491a-c30e-1c61-5c93-7c856d78de52" -auth-token="3yES71p3L97EkQkwJXvj -input="/home/reak-1/NetBeansProjects/php_codes/php_python_execute/twgzshnilfjaypmcrkevodqbxu.csv" -output="/home/reak-1/NetBeansProjects/php_codes/php_python_execute/output-twgzshnilfjaypmcrkevodqbxu.csv" -log="log.txt"'])

def insert_origininal(sheet_name):
        
        sheet_id = sheet_idfromname(sheet_name)
        
        with open(path+sheet_name, 'r') as f:
            reader =  csv.reader(f, delimiter='\n')    
            for row in reader:
                   for data in row:
                       if 'Company_name,Category,Address,City' not in data:
                            r=data.split(',')  
                                
        
                            db = MySQLdb.connect("localhost","root","reak","connected_fibre" )
                            cursor = db.cursor()
                            sql = "INSERT INTO `original_data`(`sheet_id`, `company_name`) VALUES ('%s', '%s')" % (sheet_id, r[0]) 
                            try:
                                    # Execute the SQL command
                                    cursor.execute(sql)
                                    # Commit your changes in the database
                                    db.commit()
                                    print("Inserted in Original Data")
                                    process(sheet_name, "20")

                                    insert_smarty_streets(sheet_name, sheet_id)

                                    #create_smarty_streets(sheet_name)


                            except:
                                    # Rollback in case there is any error
                                    #db.rollback()
                                    print("Rolling Back DB")
                            # disconnect from server
                            db.close()
                            print("End Func")
        
def insert_smarty_streets(sheet_name, sheet_id):
        with open(path+"output-"+sheet_name, 'r') as f:
            reader =  csv.reader(f, delimiter='\n')    
            for row in reader:
                   for data in row:
                       if 'Company_name,Category,Address,City' not in data:
                            r=data.split(',') 
    
                            db = MySQLdb.connect("localhost","root","reak","connected_fibre" )
                            cursor = db.cursor()
                            sql = "INSERT INTO `smartystreets_data`(`sheet_id`, `Company_name`) VALUES ('%s', '%s')" % (sheet_id, r[0]) 
                            try:
                                    # Execute the SQL command
                                    cursor.execute(sql)
                                    # Commit your changes in the database
                                    db.commit()
                                    print("Smarty Street Inserted Successfully")
                                    process(sheet_name, "60")
                                    insert_near_net(sheet_name, "5")


                            except:
                                    # Rollback in case there is any error
                                    #db.rollback()
                                    print("Rolling Back DB")
                            # disconnect from server
                            db.close()
                            print("End Func")
    
def insert_near_net(sheet_name, sheet_id):
    	db = MySQLdb.connect("localhost","root","reak","connected_fibre" )
	cursor = db.cursor()
	sql = "INSERT INTO `near_net`(`sheet_id`) VALUES ('%s')" % (sheet_id) 
	try:
		# Execute the SQL command
		cursor.execute(sql)
		# Commit your changes in the database
		db.commit()
		print("Near Net Added Successfully")
                process(sheet_name, "80")
                insert_on_net(sheet_name, "5")
                
	except:
		# Rollback in case there is any error
		#db.rollback()
		print("Rolling Back DB")
	# disconnect from server
	db.close()
	print("End Func")
        
def insert_on_net(sheet_name, sheet_id):
    	db = MySQLdb.connect("localhost","root","reak","connected_fibre" )
	cursor = db.cursor()
	sql = "INSERT INTO `on_net`(`sheet_id`) VALUES ('%s')" % (sheet_id) 
	try:
		# Execute the SQL command
		cursor.execute(sql)
		# Commit your changes in the database
		db.commit()
		print("On Net added Successfully")
                process(sheet_name, "100")
	except:
		# Rollback in case there is any error
		#db.rollback()
		print("Rolling Back DB")
	# disconnect from server
	db.close()
	
        
        
	
'''
Main
'''	


#addproductindb("500", "sheet ka name", "smarty name", "150245343", "1000", "100%")



sheet_name = "twgzshnilfjaypmcrkevodqbxu.csv"
path = "/home/reak-1/NetBeansProjects/php_codes/php_python_execute/"
insert_origininal(sheet_name)
#create_smarty_streets(sheet_name)
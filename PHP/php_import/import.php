<?php
$array = array("emp_no"=>"12345","first_name"=>"arjana","last_name"=>"Patel");
$json_data =  json_encode($array);

$file = fopen("employee.csv","r");
$flag = 0;
$fields = "";
$data = array();
while(! feof($file))
  {
      if($flag == 0)
      {
          // get field name from csv
        $fields = (fgetcsv($file));
        $flag = $flag+1;
      }
      else{
           //get data here and add in db 
          
          $fielddata = fgetcsv($file);
          foreach($fields as $key=>$fieldName)
          {
            if($key == 0)
            {

            }else{
                
                if($fieldName == "employee hashkey")
                {
                    $fieldName = "employeehashkey";
                }
                if($fieldName == "dept_job no")
                {
                    $fieldName = "dept_jobno";
                } if($fieldName == "dept_job name")
                {
                    $fieldName = "dept_jobname";
                }
                if($fieldName == "employee Signature")
                {
                    $fieldName = "employeeSignature";
                }
                if($fieldName == "project hashkey")
                {
                    $fieldName = "projecthashkey";
                }
                $tmp[$fieldName] = $fielddata[$key]; 
            }
            
          }
          
         array_push($data,$tmp);
         
      }
   // echo json_encode(fgetcsv($file));
      
  }

Validation($data);
function Validation($data)
{
    // execute data and check validity for each column 
    //if column data not available store default values
    
    foreach($data as $keys=>$rowData)
    {
        foreach($rowData as $key=>$value)
         {
            if($key == "employeehashkey")
            {
                $data[$keys][$key] = sha1($data[$keys]["first_name"].
                                            $data[$keys]["last_name"].
                                            $data[$keys]["mobile"].
                                            $data[$keys]["gender"].
                                            $data[$keys]["date_of_birth"].
                                            strtotime("now"));
            }
            if($key == "emp_no" && $value == "")
            {
                $data[$keys][$key] = "12345";
            }
            if($key == "registration_type")
            {
                 $data[$keys][$key] = "NewHire";
                
            }
            if($key == "job_type")
            {
                $data[$keys][$key] = "full time";
            }
            if($key == "gender")
            {
                $data[$keys][$key] = lcfirst($value);
            }
            if($key == "date_of_birth")
            {
                if(strpos($value,"@")>0)
                {
                    $data[$keys][$key] = "1/1/1991";
                }
            }
            if($key == "supervisor")
            {
                $data[$keys][$key] = "Thomas O'Brien";
            }
            if($key == "supervisor_id")
            {
                $data[$keys][$key] = 1;
            }
            if($key == "marital_status")
            {
                if($value != "married" || $value == "")
                {
                    $data[$keys][$key] = "single";
                }
            }
            if($key == "type")
            {
                $data[$keys][$key] = "employee";
            }
            if($key == "timestamp")
            {
                $data[$keys][$key] = strtotime("now");
            }
            if($key == "vp_approval")
            {
                $data[$keys][$key] = 0;
            }
            if($key == "phone")
            {
                $data[$keys][$key] = str_replace("(","",$value);
                $data[$keys][$key] = str_replace(")","",$data[$keys][$key]);
                $data[$keys][$key] = str_replace("-","",$data[$keys][$key]);
                $data[$keys][$key] = str_replace(" ","",$data[$keys][$key]);
            }
            if($key == "mobile")
            {
                $data[$keys][$key] = str_replace("(","",$value);
                $data[$keys][$key] = str_replace(")","",$data[$keys][$key]);
                $data[$keys][$key] = str_replace("-","",$data[$keys][$key]);
                $data[$keys][$key] = str_replace(" ","",$data[$keys][$key]);
            }
            if($key == "emergency_phone_no")
            {
                $data[$keys][$key] = str_replace("(","",$value);
                $data[$keys][$key] = str_replace(")","",$data[$keys][$key]);
                $data[$keys][$key] = str_replace("-","",$data[$keys][$key]);
                $data[$keys][$key] = str_replace(" ","",$data[$keys][$key]);
            }
            if($key == "vp_approval")
            {
                $data[$keys][$key] = 0;
            }
        }
        addDataDb($data[$keys]);
        
    }//foreach
    
}//function

function addDataDb($data)
{
    $conn = new mysqli("localhost", "root", "reak", "rbtBackend");
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        echo "fail"; 
    } 
    $sqlField = "";
    $sqlValues = "";
    foreach($data as $key=>$value)
    {
        
        if($key!= "supervisorSignature")
        {
            $sqlField = $sqlField.$key.",";
            if($value=="")
            {
                $sqlValues = $sqlValues."null".",";
            }else{
                $sqlValues = $sqlValues.'"'.$value.'",';
            }
            
        }else{
            $sqlField = $sqlField.$key;
            $sqlValues = $sqlValues."null";
        }
    }
    echo '<br>';    
     $sql = "INSERT INTO employee (".$sqlField.") VALUES (".$sqlValues.")";
    echo '<hr>';
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    
    mysqli_close($conn);
 
}



fclose($file);
?>
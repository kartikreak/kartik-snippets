<?php
$n = 10; 
fibonacci($n); 


function fibonacci($n)
{
     // Starting clock time in seconds 
     $start_time = microtime(true); 
     $a=1; 

     $num1 = 0; 
     $num2 = 1; 
   
     $counter = 0; 
     while ($counter < $n){ 
         echo ' '.$num1; 
         $num3 = $num2 + $num1; 
         $num1 = $num2; 
         $num2 = $num3; 
         $counter = $counter + 1; 
         $a++;
     } 

     // End clock time in seconds 
     $end_time = microtime(true); 

     // Calculate script execution time 
     $execution_time = ($end_time - $start_time); 
     echo "<br/>";
     echo " Execution time of script = ".$execution_time." sec"; 
     echo "<br/>";
     echo "memory (byte): ", memory_get_peak_usage(true), "\n";
}
?>
<?php
header("Access-Control-Allow-Origin: *");
$array = array(
    'status' => 'success',
    'fundingRounds' => '5',
    'fundingAmount' => '$100000',    
    'fundingData' => array(
        array('year' => '2010', 'value' => '150'),
        array('year' => '2011', 'value' => '240'),
        array('year' => '2012', 'value' => '100.50'),
        array('year' => '2013', 'value' => '50'),
        array('year' => '2014', 'value' => '500'),
        array('year' => '2015', 'value' => '300.5'),
        array('year' => '2016', 'value' => '90'),
        array('year' => '2017', 'value' => '200'),
        array('year' => '2018', 'value' => '120'),
    ),
 );
 echo (json_encode($array));
?>
<?php
header("Access-Control-Allow-Origin: *");
$arrayRandom =array("");
function shuffleRandom()
{
    return rand(10,200);
}
$array = array(
    'status' => 'success',
    'industryAnalysisLeftGraphData' => array(
        array('year' => '2010', 'value' => shuffleRandom()),
        array('year' => '2011', 'value' => shuffleRandom()),
        array('year' => '2012', 'value' => shuffleRandom()),
        array('year' => '2013', 'value' => shuffleRandom()),
        array('year' => '2014', 'value' => shuffleRandom()),
        array('year' => '2015', 'value' => shuffleRandom()),
        array('year' => '2016', 'value' => shuffleRandom()),
        array('year' => '2017', 'value' => shuffleRandom()),
    ),
    'industryAnalysisLeftPannelGrowthCalculation' => array("sectorGrowth"=>"110%",
    "yearOfYearGrowth"=>"20%",
    "projectedGrowth"=>"5%",
    "newSectors"=>"3",
    "newCompanies"=>"20")
 );
 echo (json_encode($array));
?>
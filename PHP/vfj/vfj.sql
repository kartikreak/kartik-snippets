-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 26, 2019 at 04:01 PM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vfj`
--

-- --------------------------------------------------------

--
-- Table structure for table `anomaly`
--

CREATE TABLE `anomaly` (
  `id` int(11) NOT NULL,
  `emp_type` varchar(255) DEFAULT NULL,
  `from_date` varchar(255) DEFAULT NULL,
  `to_date` varchar(255) DEFAULT NULL,
  `section_from` int(11) DEFAULT NULL,
  `section_to` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anomaly`
--

INSERT INTO `anomaly` (`id`, `emp_type`, `from_date`, `to_date`, `section_from`, `section_to`, `status`) VALUES
(1, 'type3', '2019-06-02', '2019-06-15', 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `delegation`
--

CREATE TABLE `delegation` (
  `id` int(11) NOT NULL,
  `year` int(11) DEFAULT NULL,
  `month` varchar(100) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `personal` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `attach_section_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delegation`
--

INSERT INTO `delegation` (`id`, `year`, `month`, `section_id`, `emp_id`, `personal`, `type`, `attach_section_id`, `status`) VALUES
(1, 2020, 'Mar', 1, 8, '1212', 'typ', 1, 1),
(2, 2014, 'May', 1, 9, 'kjjhb3654', 'jhjh', 3, 1),
(3, 2015, 'Nov', 1, 4, 'jhgjhgf', 'mbjhgbjhgb', 3, 1),
(5, 1245, 'Mar', 1, 4, '65hkj', 'hbvhvf  jhjkh', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `forgotPassword`
--

CREATE TABLE `forgotPassword` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gang`
--

CREATE TABLE `gang` (
  `id` int(11) NOT NULL,
  `gang_no` varchar(255) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `gpw_dw_ipw` varchar(255) DEFAULT NULL,
  `date_from` varchar(255) DEFAULT NULL,
  `date_to` varchar(255) DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gang`
--

INSERT INTO `gang` (`id`, `gang_no`, `section_id`, `gpw_dw_ipw`, `date_from`, `date_to`, `timestamp`, `status`) VALUES
(2, '111', 1, 'IPW', '2019-06-02', '2019-06-08', '1561444604', '1');

-- --------------------------------------------------------

--
-- Table structure for table `gang_emp`
--

CREATE TABLE `gang_emp` (
  `id` int(11) NOT NULL,
  `gang_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gang_emp`
--

INSERT INTO `gang_emp` (`id`, `gang_id`, `emp_id`) VALUES
(3, 2, 10),
(4, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `grace_time`
--

CREATE TABLE `grace_time` (
  `id` int(11) NOT NULL,
  `time` varchar(255) DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grace_time`
--

INSERT INTO `grace_time` (`id`, `time`, `timestamp`) VALUES
(1, '00:15', '1559730986');

-- --------------------------------------------------------

--
-- Table structure for table `leave_assign`
--

CREATE TABLE `leave_assign` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `leave_code_id` int(11) DEFAULT NULL,
  `absent_date` varchar(255) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `work_on_date` varchar(255) DEFAULT NULL,
  `late_indicator` varchar(10) DEFAULT NULL,
  `c_l` varchar(10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_assign`
--

INSERT INTO `leave_assign` (`id`, `emp_id`, `leave_code_id`, `absent_date`, `shift_id`, `work_on_date`, `late_indicator`, `c_l`, `status`) VALUES
(1, 9, 7, '2019-06-02', 2, '2019-06-11', 'yes', 'no', 0),
(2, 10, 3, '2019-05-02', 3, '2019-06-06', 'no', 'yes', 0),
(3, 10, 3, '2019-05-08', 6, '2019-06-19', 'yes', 'yes', 0),
(4, 10, 6, '2019-05-03', 3, '2019-06-10', 'yes', 'yes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `leave_codes`
--

CREATE TABLE `leave_codes` (
  `id` int(11) NOT NULL,
  `leave_code` varchar(255) DEFAULT NULL,
  `leave_detail` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_codes`
--

INSERT INTO `leave_codes` (`id`, `leave_code`, `leave_detail`, `status`) VALUES
(1, '1', 'Suspend', 1),
(2, 'B', 'HPL with mc_code 1', 1),
(3, 'C', 'Casual Leave', 1),
(4, 'D', 'NRB Gate Pass', 1),
(5, 'E', 'Earned Leave', 1),
(6, 'H', 'Half Pay Leave', 1),
(7, 'I', 'Injury Leave', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `email` varchar(255) NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`email`, `timestamp`) VALUES
('arj@reak.in', '1559040144'),
('raj@reak.in', '1559282118'),
('admin@reak.in', '1560946653');

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL,
  `starting_date` varchar(255) DEFAULT NULL,
  `ending_date` varchar(255) DEFAULT NULL,
  `employee_type_id` varchar(255) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `ot_limit` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overtime`
--

INSERT INTO `overtime` (`id`, `starting_date`, `ending_date`, `employee_type_id`, `section_id`, `ot_limit`, `status`) VALUES
(1, '2019-06-02', '2019-06-08', 'type2', 1, '5400', 0);

-- --------------------------------------------------------

--
-- Table structure for table `overtime_emp`
--

CREATE TABLE `overtime_emp` (
  `id` int(11) NOT NULL,
  `overtime_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `ot_0` varchar(255) DEFAULT NULL,
  `ot_1` varchar(255) DEFAULT NULL,
  `ot_2` varchar(255) DEFAULT NULL,
  `ot_3` varchar(255) DEFAULT NULL,
  `ot_4` varchar(255) DEFAULT NULL,
  `ot_5` varchar(255) DEFAULT NULL,
  `ot_6` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overtime_emp`
--

INSERT INTO `overtime_emp` (`id`, `overtime_id`, `emp_id`, `ot_0`, `ot_1`, `ot_2`, `ot_3`, `ot_4`, `ot_5`, `ot_6`) VALUES
(1, 1, 10, '00:00', '01:00', '01:00', '01:00', '01:00', '01:00', '04:15'),
(2, 1, 4, '00:00', '01:00', '0101', '01:00', '01:00', '01:00', '04:15'),
(3, 1, 9, '00:00', '01:00', '01:00', '01:00', '01:00', '01:00', '04:15');

-- --------------------------------------------------------

--
-- Table structure for table `punch`
--

CREATE TABLE `punch` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `punch_status` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `punch`
--

INSERT INTO `punch` (`id`, `date`, `time`, `timestamp`, `emp_id`, `punch_status`, `status`) VALUES
(1, '2019-05-01', '05:56:00', '1556670360', 10, 'checkIn', 0),
(2, '2019-05-01', '06:00:00', '1556670600', 10, 'checkOut', 0),
(3, '2019-05-02', '06:00:00', '1556757000', 10, 'checkIn', 0),
(4, '2019-05-02', '15:00:00', '1556789400', 10, 'checkOut', 0),
(5, '2019-05-03', '06:00:00', '1556843400', 10, 'checkIn', 0),
(6, '2019-05-04', '06:00:00', '1556929800', 10, 'checkIn', 0),
(7, '2019-05-04', '15:00:00', '1556962200', 10, 'checkOut', 0);

-- --------------------------------------------------------

--
-- Table structure for table `raw_punch`
--

CREATE TABLE `raw_punch` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `raw_punch`
--

INSERT INTO `raw_punch` (`id`, `date`, `time`, `timestamp`, `emp_id`, `status`) VALUES
(1, '2019-05-01', '15:00:00', '1556703000', 10, 0),
(2, '2019-05-01', '15:05:00', '1556703300', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `section_code` varchar(255) DEFAULT NULL,
  `section_type` varchar(255) DEFAULT NULL,
  `timestamp` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `section_name`, `section_code`, `section_type`, `timestamp`, `status`) VALUES
(1, 'Section 1', 'A', 'DayWork', '1561384216', 1),
(2, 'Section 2', 'B', 'PieceWork', '1561384240', 1),
(3, 'Section 3', 'C', 'Staff', '1561384258', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(255) DEFAULT NULL,
  `shift_type` varchar(255) DEFAULT NULL,
  `over_day` int(11) DEFAULT NULL,
  `from_time` varchar(255) DEFAULT NULL,
  `to_time` varchar(255) DEFAULT NULL,
  `lunch_from` varchar(255) DEFAULT NULL,
  `lunch_to` varchar(255) DEFAULT NULL,
  `duty_hours` varchar(255) DEFAULT NULL,
  `lunch_hours` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `shift_name`, `shift_type`, `over_day`, `from_time`, `to_time`, `lunch_from`, `lunch_to`, `duty_hours`, `lunch_hours`, `gender`, `status`) VALUES
(2, 'A', 'normal', 0, '06:00', '14:30', '10:00', '10:30', '8', '0.5', 'female', 1),
(3, 'B', 'normal', 0, '06:00', '15:00', '11:00', '12:00', '8', '1', 'male', 1),
(4, 'a', 'saturday', 0, '06:00', '10:45', '10:45', '11:15', '4.25', '0.5', 'female', 0),
(5, 'b', 'saturday', 0, '06:00', '10:45', '10:45', '11:45', '3.75', '1', 'male', 0),
(6, 'S', 'normal', 1, '20:00', '05:00', '00:30', '01:30', '8', '1', 'male', 1),
(7, 's', 'saturday', 1, '20:00', '00:45', '00:45', '01:45', '4.75', '1', 'female', 0),
(8, 'C', 'normal', 0, '07:00', '16:00', '12:00', '13:00', '8', '1', 'male', 1),
(9, 'c', 'saturday', 0, '07:00', '11:45', '11:45', '12:45', '3.75', '1', 'female', 0),
(10, 'D', 'normal', 0, '07:30', '16:30', '12:30', '13:30', '8', '1', 'male', 1),
(11, 'd', 'saturday', 0, '07:30', '12:15', '12:15', '13:15', '3.75', '1', 'male', 0),
(12, 'e', 'saturday', 0, '07:45', '12:30', '12:30', '13:00', '4.25', '0.5', 'Female', 0),
(13, 'E', 'normal', 0, '07:45', '16:15', '12:30', '13:00', '8', '0.5', 'Female', 1),
(14, 'R', 'normal', 1, '19:30', '04:30', '00:30', '01:30', '8', '1', 'male', 1),
(15, 'r', 'saturday', 1, '19:30', '00:15', '00:15', '01:15', '4.75', '1', 'male', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shift_assign`
--

CREATE TABLE `shift_assign` (
  `id` int(11) NOT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `date_from` varchar(255) DEFAULT NULL,
  `date_to` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift_assign`
--

INSERT INTO `shift_assign` (`id`, `shift_id`, `date_from`, `date_to`, `status`) VALUES
(2, 3, '2019-05-01', '2019-05-04', 1),
(3, 6, '2019-05-05', '2019-05-11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shift_assign_emp`
--

CREATE TABLE `shift_assign_emp` (
  `id` int(11) NOT NULL,
  `shift_assign_id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift_assign_emp`
--

INSERT INTO `shift_assign_emp` (`id`, `shift_assign_id`, `emp_id`) VALUES
(2, 2, 9),
(3, 2, 10),
(4, 2, 4),
(5, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `emp_no` varchar(255) DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `section_id` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `emp_type` varchar(255) DEFAULT NULL,
  `timestamp` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `emp_no`, `ticket`, `section_id`, `email`, `password`, `name`, `gender`, `type`, `emp_type`, `timestamp`, `status`) VALUES
(1, '1', '14155', '1', 'admin@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin', 'female', 'admin', NULL, NULL, 1),
(2, '2', 'lk785', '1', 'kartik.swamy@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Kartik', 'male', 'section_admin', '', NULL, 1),
(3, '3', '123qwe', '1', 'kartik.swamy123@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'aaa', 'female', 'section_admin', NULL, NULL, 1),
(4, '4', '345sss', '1', 'b@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'bbb test', 'male', 'section_employee', NULL, '1558515148', 1),
(5, '5', '4444', '1', 'raj@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'raj', 'male', 'section_admin', '', '1558693796', 1),
(6, '6', '26', '1', 'arj@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'arj', 'female', 'section_employee', '', '1558693952', 1),
(7, '7', '65', '1', 'arj11@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'arj11', 'female', 'section_employee', '', '1558693952', 1),
(8, '8', '636', '1', 'bname@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'bname name', 'male', 'section_employee', '', '1558947920', 1),
(9, '14465', '354654', '1', 'deep@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'dEEPSHIKHA', 'female', 'section_employee', NULL, '1559206371', 1),
(10, '56546', '236', '1', 'kartik.swamy55@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Rajesh', 'female', 'section_employee', 'type3', '1559560699', 1),
(11, '654654654654', 'jhgjhugjhgjhg', '1', 'jhgjhgjhgjhg@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'jhgjhgjhgjg', 'female', 'section_employee', 'type1', '1559563645', 1),
(12, '54664654', '354', '1', 'arj@reak.injh', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'uigtjuhgfujgjh', 'male', 'section_employee', 'type1', '1559563732', 0),
(14, '1234', '3333', '1', 'kartik.swamy66@reak.in', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Baba', 'male', 'section_admin', '', '1561441558', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anomaly`
--
ALTER TABLE `anomaly`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delegation`
--
ALTER TABLE `delegation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgotPassword`
--
ALTER TABLE `forgotPassword`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gang`
--
ALTER TABLE `gang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gang_emp`
--
ALTER TABLE `gang_emp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grace_time`
--
ALTER TABLE `grace_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_assign`
--
ALTER TABLE `leave_assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_codes`
--
ALTER TABLE `leave_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime_emp`
--
ALTER TABLE `overtime_emp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `punch`
--
ALTER TABLE `punch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raw_punch`
--
ALTER TABLE `raw_punch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift_assign`
--
ALTER TABLE `shift_assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift_assign_emp`
--
ALTER TABLE `shift_assign_emp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anomaly`
--
ALTER TABLE `anomaly`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delegation`
--
ALTER TABLE `delegation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `forgotPassword`
--
ALTER TABLE `forgotPassword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gang`
--
ALTER TABLE `gang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gang_emp`
--
ALTER TABLE `gang_emp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `grace_time`
--
ALTER TABLE `grace_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leave_assign`
--
ALTER TABLE `leave_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `leave_codes`
--
ALTER TABLE `leave_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `overtime_emp`
--
ALTER TABLE `overtime_emp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `punch`
--
ALTER TABLE `punch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `raw_punch`
--
ALTER TABLE `raw_punch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `shift_assign`
--
ALTER TABLE `shift_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shift_assign_emp`
--
ALTER TABLE `shift_assign_emp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require './db.php';
$conn = database_con();
$from_date = $to_date = "";
if ($_SERVER["REQUEST_METHOD"] == "GET") 
{
    $from_date = $_GET["from_date"];
    $to_date = $_GET["to_date"];
}
// $from_date = "2019-07-01";
// $to_date = "2019-07-30";


$data = fetch_data($from_date, $to_date, $conn);
$count = count($data);
if ($count >0) 
{
    echo json_encode($data);
}
else
{
    echo "no data";
}


function fetch_data($from_date, $to_date, $conn)
{    
    $store = array();

    $stmt = $conn->prepare("SELECT * FROM leave_transactions WHERE application_date between ? AND ?");
    $stmt->bind_param("ss", $from_date, $to_date);

    if ($stmt->execute()) {
            
                foreach ($stmt->get_result() as $row)
                {
                    array_push($store,$row);
                            
                }  
                return $store;
    }
    $stmt->close();
    $conn->close();
}



 


?>
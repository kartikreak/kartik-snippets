<?php
require './db.php';

$path = "sample.txt";
$myfile = fopen($path, "r") or die("Unable to open file!");
$counter = 0;
$res = "false";
date_default_timezone_set('GMT');

// Output one line until end-of-file
while(!feof($myfile)) 
{
    $punch_status = "";         
    $file_data = fgets($myfile);

    // EXPLODING BY TAB
    $array_data=explode("\t", $file_data);  
    
    // converting it to timestamp & time from Date
    $data = get_timestamp_data($array_data[1]);

    $emp_id = $array_data[0];
    $date = $data["date"];
    $time = $data["time"];
    $timestamp = $data["timestamp"];
    $punch = $timestamp;

    $present_day = week_days($date);

    $shiftTime = array();


    $shift_data = find_shift($date, $emp_id);
    
    

    // echo $shift_data["shift_name"];
    if ($present_day == "Saturday") 
    {        
        # code...
        $shift_name = strtolower($shift_data["shift_name"]);
        $shift_type = "saturday";
        $shift_data = shift_details($shift_name, $shift_type);        
    }
    

    $prev_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
    $prev_day = date('Y-m-d', strtotime('-1 day', strtotime($prev_date)));
    $shift_data2 = find_shift($prev_date, $emp_id);
    if ($prev_day == "Saturday") 
    {        
        # code...
        $shift_name2 = strtolower($shift_data2["shift_name"]);
        $shift_type2 = "saturday";
        $shift_data2 = shift_details($shift_name2, $shift_type2);        
    }

    
    
    if ($shift_data2 != "no") {
        # code...
        $shift_name2 = $shift_data2["shift_name"];
        $from_time2 = $shift_data2["from_time"];
        $to_time2 = $shift_data2["to_time"];

        array_push($shiftTime, $from_time2, $to_time2);        
        
    } else {
        # code...
        $shift_name2 = "";
        $from_time2 = "00:00";
        $to_time2 = "00:00";

        array_push($shiftTime, $from_time2, $to_time2);
    }

    
    // exit();

    // echo json_encode($shift_data);
    $shift_name = $shift_data["shift_name"];
    $from_time = $shift_data["from_time"];
    $to_time = $shift_data["to_time"];
    $over_day = $shift_data["over_day"];

    array_push($shiftTime, $from_time, $to_time);
    

    // print_r($shiftTime);    
    // exit();
    // $shiftTime = ['20:00','02:00','20:00','02:00'];

    $final = array();

    echo "Punch = ".$punch;
    echo "<br>";
    echo "Time = ".$time;
    echo "<br>";
    echo "Date = ".$date;
    echo "<br>";
    echo "Overday = ".$over_day;
    echo "<br>";

    
    identifyPunch($punch,$date,$shiftTime);    
    echo "Result -> ".(returnString(getClosest($punch, $final)));
    echo "<hr>";

    print_r($shiftTime);

    echo "<hr>";

}

function identifyPunch($punch, $todayDate, $shiftTime){

    global $final;

    // Match with in-time and find nearest among -1 and today

    for($i=0;$i<4;$i++){

        if($shiftTime[$i] != "00:00"){

            $hourPart = explode(":",$shiftTime[$i]);

            $num = strtotime($todayDate." ".$shiftTime[$i]);

            if(($i%2 == 1) && ((int)$hourPart[0] <= 6)){

                $num+= 86400;

            }

            if($i < 2){

                // Add 1 day to the today's date

                array_push($final, ($num-86400));

            } else {

                array_push($final, $num);

            }

        } else {

            // Hack, set day as 1st January 1970 for max difference

            array_push($final, 0);

        }

    }

}
 
 function getClosest($search, $arr) {
    $closest = array(null, null);
    foreach ($arr as $key=>$item) {
       if ($closest[1] === null || abs($search - $closest[1]) > abs($item - $search)) {
          $closest[1] = $item;
          $closest[0] = $key;
       }
    }
    return $closest;
 }
 
 function returnString($key){
    switch($key[0]){
        case "0": return "Punch IN for -1 day"; break;
        case "1": return "Punch OUT for -1 day"; break;
        case "2": return "Punch IN for today"; break;
        case "3": return "Punch OUT for today"; break;
    }
 }

 function get_timestamp_data($date)
{
    $date_time = explode(' ',$date);        
    $timestamp = (strtotime($date));
    return $temp_array = array("date"=>$date_time[0], "time"=>$date_time[1], "timestamp"=>$timestamp);
}

function find_shift($start_date, $emp_id)
{    
    $conn = database_con();    
       
    $sql = 'SELECT shift.shift_name, shift.shift_type, shift.from_time, shift.to_time, shift.lunch_from, shift.lunch_to, shift.over_day FROM `shift_assign` AS sa INNER JOIN shift AS shift ON sa.shift_id = shift.id INNER JOIN shift_assign_emp AS sae ON sae.shift_assign_id = sa.id WHERE "'.$start_date.'" BETWEEN sa.date_from AND COALESCE(sa.date_to, NOW()) AND sae.emp_id = '.$emp_id;
    
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {            
            return $row;
        }
    } else {
        // echo "0 results";
        return "no";
    }
    $conn->close();
    
}

function week_days($start_date)
    {
        //Convert the date string into a unix timestamp.
        $unixTimestamp = strtotime($start_date);
        
        //Get the day of the week using PHP's date function.
        $dayOfWeek = date("l", $unixTimestamp);
        
        //Print out the day that our date fell on.
        return $dayOfWeek;

    }

function shift_details($shift_name, $shift_type)
{        
    
    
    $conn = database_con();    
       
    $sql = 'SELECT * from shift WHERE shift_name = "'.$shift_name.'" AND shift_type = "'.$shift_type.'"';
    
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {            
            return $row;
        }
    } else {
        // echo "0 results";
        return "no";
    }
    $conn->close();

}    

?>
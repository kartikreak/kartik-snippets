
<?php

$permalinks = explode("/",$_SERVER['REQUEST_URI']);

$varone = $permalinks[1];
$vartwo = $permalinks[2];

print_r($varone);
print_r($vartwo);


?>
<html>
    <head>
        <title>Nick Mail</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
        <style>
            @import "font-awesome.min.css";
@import "font-awesome-ie7.min.css";
/* Space out content a bit */
body {
  padding-top: 20px;
  padding-bottom: 20px;
    background: url(img/nick.jpg); 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  
}

/* Everything but the jumbotron gets side spacing for mobile first views */
.header,
.marketing,
.footer {
  padding-right: 15px;
  padding-left: 15px;
}

/* Custom page header */
.header {
  border-bottom: 1px solid #e5e5e5;
}
/* Make the masthead heading the same height as the navigation */
.header h3 {
  padding-bottom: 19px;
  margin-top: 0;
  margin-bottom: 0;
  line-height: 40px;
}

/* Custom page footer */
.footer {
  padding-top: 19px;
  color: #777;
  border-top: 1px solid #e5e5e5;
}

/* Customize container */
@media (min-width: 768px) {
  .container {
    max-width: 730px;
  }
}
.container-narrow > hr {
  margin: 30px 0;
}

/* Main marketing message and sign up button */
.jumbotron {
  text-align: center;
  border-bottom: 1px solid #e5e5e5;
}
.jumbotron .btn {
  padding: 14px 24px;
  font-size: 21px;
}

/* Supporting marketing content */
.marketing {
  margin: 40px 0;
}
.marketing p + h4 {
  margin-top: 28px;
}

/* Responsive: Portrait tablets and up */
@media screen and (min-width: 768px) {
  /* Remove the padding we set earlier */
  .header,
  .marketing,
  .footer {
    padding-right: 0;
    padding-left: 0;
  }
  /* Space out the masthead */
  .header {
    margin-bottom: 30px;
  }
  /* Remove the bottom border on the jumbotron for visual effect */
  .jumbotron {
    border-bottom: 0;
  }
}
        </style>
    </head>
    <body>
        <div class="container">
            <h1 class="well">Revolution Solar <span><img src="img/logo-white.png" style="margin-top: -15px;" class="pull-right" alt="No Image"></span></h1>
	<div class="col-lg-12 well">
	<div class="row">
            <form method="post" action="mail.php">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>First Name</label>
								<input type="text" id="first1" name="first1" placeholder="First" class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>Last Name</label>
								<input type="text" id="last1" name="last1" placeholder="Last" class="form-control">
							</div>
						</div>	
                                                <div class="row">
							<div class="col-sm-6 form-group">
								<label>First Name</label>
								<input type="text" id="first2" name="first2" placeholder="First" class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>Last Name</label>
								<input type="text" id="last2" name="last2" placeholder="Last" class="form-control">
							</div>
                                                        <div class="col-sm-6 form-group">
                                                        <label>Phone Number</label>
                                                        <input type="text" id="phone" name="phone" placeholder="### ### ###" class="form-control">
                                                        </div>
						</div>	
	
						<div class="form-group">
							<label>Address</label>
                                                        <input type="text" id="address" name="address" placeholder="Street Address" class="form-control">
							
						</div>	
						<div class="row">
							<div class="col-sm-6 form-group">
								
								<input type="text" id="city" name="city" placeholder="City" class="form-control">
							</div>	
							<div class="col-sm-6 form-group">
								
								<input type="text" id="state" name="state" placeholder="State/Provision/Region" class="form-control">
							</div>	
							<div class="col-sm-6 form-group">
								
								<input type="text" id="zip" name="zip" placeholder="Postal /Zip Code" class="form-control">
							</div>		
						</div>						
					<div class="form-group">
						<label>Email Address</label>
						<input type="text" id="email" name="email" placeholder="Email Address" class="form-control">
					</div>	
                         		<div class="row">
					<div class="col-sm-6 form-group">
						<label>Roof Condition</label>						                                                
                                                <select class="form-control" id="roof" name="roof">
                                                    <option value="poor">Poor</option>
                                                    <option value="fair">Fair</option>
                                                    <option value="average">Average</option>
                                                    <option value="good">Good</option>
                                                    <option value="good">Excellent</option>                                                    
                                                  </select>
					</div>
                                        <div class="col-sm-6 form-group">
						<label>Credit Score</label>						                                                
                                                <select class="form-control" id="credit" name="credit">
                                                    <option value="600">600</option>
                                                    <option value="620">620</option>
                                                    <option value="640">640</option>
                                                    <option value="660">660</option>
                                                    <option value="700+">700+</option>
                                                </select>
					</div>    
                                        <div class="col-sm-6 form-group">
						<label>Best Time to Contact</label>						                                                
                                                <select class="form-control" id="time" name="time">
                                                    <option value="morning">Morning</option>
                                                    <option value="afternoon">Afternoon</option>
                                                    <option value="evening">Evening</option>
                                                </select>
					</div>  
                                        <div class="col-sm-6 form-group">
						<label>Select Email</label>
                                                <input type="text" id="select_email" name="select_email" placeholder="selected email" class="form-control" value="@revolutionsolar.energy">
                                                <select class="form-control">
                                                    <option value="nick@revolutionsolar.energy">nick(at)revolutionsolar.energy</option>
                                                    <option value="matt@revolutionsolar.energy">matt(at)revolutionsolar.energy</option>
                                                    <option value="Patrick@revolutionsolar.energy">Patrick(at)revolutionsolar.energy</option>
                                                    <option value="ben@revolutionsolar.energy">ben(at)revolutionsolar.energy</option>
                                                    <option value="shanyka@revolutionsolar.energy">shanyka(at)revolutionsolar.energy</option>
                                                </select>
					</div>     
					
						</div>	
                                    	     <div class="row">
							<div class="col-sm-8 form-group">
                                                        <label>Additional Notes</label>    
                                                        <textarea placeholder="" rows="5" class="form-control" name="notes"></textarea>
							</div>									
						</div>	
                                            <input type="submit" value="Submit" class="btn btn-lg btn-warning">    										
					</div>
				</form> 
				</div>
	</div>
	</div>
        		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>		
                        <script>
                            $('.flexdatalist').flexdatalist({
                                    minLength: 1
                               });
                        </script>
    </body>
</html>
